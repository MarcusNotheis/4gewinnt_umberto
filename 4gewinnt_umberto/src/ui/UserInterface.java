package ui;

import home.Administration;
import models.Draw;
import models.Token;

/**
 * UserInterface.java controls all other UI-classes.
 * 
 * @author Bender
 *
 */
public class UserInterface {

	private Administration administration;
	private Start start;
	private Grid grid;
	private Replay replay;

	/**
	 * Constructor to create a UserInterface object
	 * 
	 * @param administration
	 *            Administration object to get all necessary application objects
	 *            and information.
	 */
	public UserInterface(Administration administration) {
		this.administration = administration;
		start = new Start(this);
	}

	/**
	 * Returns the current Administration
	 * 
	 * @return Return value.
	 */
	public Administration getAdministration() {
		return administration;
	}

	/**
	 * Sets the current Administration class for the UserInterface.
	 * 
	 * @param administration
	 *            The current Administration class.
	 * 
	 */
	public void setAdministration(Administration administration) {
		System.out.println("UserInterface: Adminklasseninstanz = "
				+ administration);
		this.administration = administration;
	}

	/**
	 * Initializes a new Grid window.
	 * 
	 * @param opponentName
	 *            Opponent name which is going to be displayed on the UI.
	 */
	public void initializeGrid(String opponentName) {
		this.grid = new Grid(this);
		grid.changeOpponentName(opponentName);

	}

	/**
	 * Returns the current set number.
	 * 
	 * @return Return value setCount (Integer 0-3, 0 = no current set).
	 */
	public int getSetCount() {
		int returnValue = Grid.getSetCount();
		return returnValue;
	}

	/**
	 * Sets token on the grid
	 * 
	 * @param draw
	 *            Container that provides all necessary information to display a
	 *            token on the UI.
	 */
	public void setToken(Draw draw) {
		grid.setToken(draw);
	}

	/**
	 * Sets green token on the grid
	 * 
	 * @param winningToken
	 *            Container that provides all necessary information to display a
	 *            winning token on the UI.
	 */
	public void setWinningToken(Token[] winningToken) {
		grid.setWinningToken(winningToken);
	}

	/**
	 * Initializes a new Replay window.
	 * 
	 * @param game
	 *            GameID of current match.
	 */
	public void initializeReplay(String game) {
		this.replay = new Replay();
		replay.setGame(game);
	}

}
