package ui;

import home.Administration;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import models.Token;
import db.Database;

/**
 * The purpose of Grid.java is to display past matches from the database on the
 * UI.
 * 
 * @author Bender
 *
 */
public class Replay {

	private static JFrame frame;
	private static HashMap<String, Component> componentMap;
	private static DefaultListModel listModel = new DefaultListModel();
	private static ArrayList<String[]> game = new ArrayList<String[]>();
	private static String gameID;
	private static String setID = "1";
	static Point mouseDownCompCoords;

	/**
	 * Constructor to create a Replay() object with a new window.
	 * 
	 */
	public Replay() {
		initialize();
		prepareUi();
		frame.setVisible(true);
	}

	private void prepareUi() {
		resetToken();
		getGameData();
	}

	private static void getGameData() {
		Database db = new Database();
		game = db.selectAllDrawsForGameInSet(gameID, setID);
		fillListModel();
	}

	private static void fillListModel() {
		listModel.clear();
		int i = 1;
		if (game.isEmpty()) {
			listModel.addElement("Keine Züge vorhanden!");
		} else {
			for (String[] entry : game) {

				listModel.addElement("Zug " + i + " - Spieler " + entry[2]);
				i++;
			}
		}
	}

	private static boolean setToken(String[] draw) {
		int player = Integer.parseInt(draw[2]);
		int x = Integer.parseInt(draw[0]) + 1;
		int y = Integer.parseInt(draw[1]) + 1;
		JLabel token = (JLabel) getComponentByName("tok_" + x + "_" + y);

		switch (player) {
		default:
			System.err.println("Grid: Übergebene SpielerID != 1 oder 2");
			return false;
		case 1:
			token.setIcon(new ImageIcon(Grid.class
					.getResource("/img/redSmall.png")));
			return true;

		case 2:
			token.setIcon(new ImageIcon(Grid.class
					.getResource("/img/yelSmall.png")));
			return true;
		}
	}

	/**
	 * Method to set a green token on the UI which indicates a winning streak (4
	 * in a row).
	 * 
	 * @param Current
	 *            selected game.
	 */
	public void setWinningToken(int selection) {
		Database db = new Database();
		Token[] winningToken = db.getWinningTokensForGame(gameID, setID);
		if (winningToken != null) {
			System.out.println("Token1: " + winningToken[0]);
			for (Token winToken : winningToken) {
				System.out.println("Winningtoken X: "
						+ winToken.getxCoordinate() + 1);
				System.out.println("Winningtoken Y: "
						+ winToken.getyCoordinate() + 1);

				JLabel token = (JLabel) getComponentByName("tok_"
						+ (winToken.getxCoordinate() + 1) + "_"
						+ (winToken.getyCoordinate() + 1));
				token.setIcon(new ImageIcon(Grid.class
						.getResource("/img/greSmall.png")));
			}
		}
	}

	private void resetToken() {
		for (int i = 1; i <= 7; i++) {
			for (int j = 1; j <= 6; j++) {
				JLabel token = (JLabel) getComponentByName("tok_" + i + "_" + j);
				token.setIcon(null);
			}
		}
	}

	/**
	 * Initialization of all UI Elements
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(300, 200, 720, 540);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setUndecorated(true);
		frame.getContentPane().setLayout(null);
		mouseDownCompCoords = null;
		frame.addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {
				mouseDownCompCoords = null;
			}

			public void mousePressed(MouseEvent e) {
				mouseDownCompCoords = e.getPoint();
			}

			public void mouseExited(MouseEvent e) {
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseClicked(MouseEvent e) {
			}
		});

		frame.addMouseMotionListener(new MouseMotionListener() {
			public void mouseMoved(MouseEvent e) {
			}

			public void mouseDragged(MouseEvent e) {
				Point currCoords = e.getLocationOnScreen();
				frame.setLocation(currCoords.x - mouseDownCompCoords.x,
						currCoords.y - mouseDownCompCoords.y);
			}
		});
		final JButton forwardBtn = new JButton("");
		final JButton backwardBtn = new JButton("");

		ListSelectionListener listSelection = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent listSelectionEvent) {
				boolean adjust = listSelectionEvent.getValueIsAdjusting();
				if (!adjust && game.isEmpty() == false) {
					resetToken();
					JList list = (JList) listSelectionEvent.getSource();
					int selection = list.getSelectedIndex();
					for (int i = 0; i <= list.getSelectedIndex(); i++) {
						String[] draw = game.get(i);
						setToken(draw);
					}
					if (selection + 1 == listModel.getSize()) {
						setWinningToken(selection);
						forwardBtn.setEnabled(false);
					}
					if (selection == 0) {
						backwardBtn.setEnabled(false);
					}
					if (selection > 0) {
						backwardBtn.setEnabled(true);
					}
					if (selection + 1 < listModel.getSize()) {
						forwardBtn.setEnabled(true);
					}
				}
			}
		};

		final JList list = new JList(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setBackground(new Color(240, 240, 240));
		list.setBorder(null);
		list.setName("list");
		list.addListSelectionListener(listSelection);
		list.setFocusable(false);
		list.setSelectionBackground(null);
		list.setSelectionForeground(new Color(255, 0, 0));
		list.setVisible(true);

		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBackground(new Color(240, 240, 240));
		scrollPane.setFocusable(false);
		scrollPane.setBorder(null);
		scrollPane.setBounds(539, 245, 157, 270);
		frame.getContentPane().add(scrollPane);
		scrollPane.setVisible(true);

		forwardBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resetToken();
				if (game.isEmpty()) {

				} else {
					int index = list.getSelectedIndex();
					index = index + 1;
					list.ensureIndexIsVisible(list.getSelectedIndex() + 6);
					if (index <= listModel.getSize()) {
						list.setSelectedIndex(index);
					}
					System.out.println("Get Selected Index: " + index);
					System.out.println("Modelsize: " + listModel.getSize());
					if (list.getSelectedIndex() == listModel.getSize()) {
						forwardBtn.setEnabled(false);
						setWinningToken(list.getSelectedIndex());
					}
					if (index == 0) {
						backwardBtn.setEnabled(false);
					}
					if (index > 0) {
						backwardBtn.setEnabled(true);
					}
					for (int i = 0; i <= list.getSelectedIndex(); i++) {
						String[] draw = game.get(i);
						setToken(draw);
					}
					if (list.getSelectedIndex() + 1 == listModel.getSize()) {
						forwardBtn.setEnabled(false);
						setWinningToken(list.getSelectedIndex() + 1);
					}

				}
			}
		});

		JButton changeWinnerBtn = new JButton("Gewinner \u00e4ndern");
		changeWinnerBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Database db = new Database();
				String[] game = db.selectGameById(gameID);
				String opponentName = game[1];
				String winnerName = game[2];

				JRadioButton player = new JRadioButton("Umberto");
				JRadioButton opponent = new JRadioButton(opponentName);
				if (winnerName == "Umberto") {
					player.setSelected(true);
				} else {
					opponent.setSelected(true);
				}
				ButtonGroup playerSelection = new ButtonGroup();
				playerSelection.add(player);
				playerSelection.add(opponent);

				Object[] message = { "Gewinner des Spiels?", player, opponent, };
				Object[] options = { "Speichern", "Abbrechen" };

				JOptionPane inputMask = new JOptionPane(message,
						JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION,
						null, options, options[0]);
				inputMask.createDialog(null, "Gewinner \u00e4ndern")
						.setVisible(true);

				if (inputMask.getValue() == "Speichern") {
					ButtonModel selectedModel = playerSelection.getSelection();
					if (player.getModel().equals(selectedModel)
							&& player.getText() != winnerName) {
						db.updateWinner(gameID, player.getText());
					} else {
						if (opponent.getModel().equals(selectedModel)
								&& player.getText() != winnerName) {
							db.updateWinner(gameID, opponent.getText());
						}
					}

				}
			}
		});

		JButton homeBtn = new JButton("");
		homeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Start.resetList();
				new Administration();
				if (frame.isDisplayable()) {
					frame.dispose();
				}
			}
		});

		final JButton set3Btn = new JButton("");
		set3Btn.setIcon(new ImageIcon(Replay.class
				.getResource("/img/setThreeUnselected.png")));
		set3Btn.setBounds(669, 167, 30, 30);
		set3Btn.setBorder(BorderFactory.createEmptyBorder());
		set3Btn.setContentAreaFilled(false);
		set3Btn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		frame.getContentPane().add(set3Btn);

		final JButton set2Btn = new JButton("");
		set2Btn.setIcon(new ImageIcon(Replay.class
				.getResource("/img/setTwoUnselected.png")));
		set2Btn.setBounds(596, 167, 30, 30);
		set2Btn.setBorder(BorderFactory.createEmptyBorder());
		set2Btn.setContentAreaFilled(false);
		set2Btn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		frame.getContentPane().add(set2Btn);

		final JButton set1Btn = new JButton("");
		set1Btn.setBounds(526, 167, 30, 30);
		set1Btn.setIcon(new ImageIcon(Replay.class
				.getResource("/img/setOneSelected.png")));
		set1Btn.setBorder(BorderFactory.createEmptyBorder());
		set1Btn.setContentAreaFilled(false);
		set1Btn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		frame.getContentPane().add(set1Btn);

		set1Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				set3Btn.setIcon(new ImageIcon(Replay.class
						.getResource("/img/setThreeUnselected.png")));
				set2Btn.setIcon(new ImageIcon(Replay.class
						.getResource("/img/setTwoUnselected.png")));
				set1Btn.setIcon(new ImageIcon(Replay.class
						.getResource("/img/setOneSelected.png")));
				setID = "1";
				getGameData();
				backwardBtn.setEnabled(true);
				forwardBtn.setEnabled(true);
				list.clearSelection();
				resetToken();
			}
		});

		set2Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				set1Btn.setIcon(new ImageIcon(Replay.class
						.getResource("/img/setOneUnselected.png")));
				set3Btn.setIcon(new ImageIcon(Replay.class
						.getResource("/img/setThreeUnselected.png")));
				set2Btn.setIcon(new ImageIcon(Replay.class
						.getResource("/img/setTwoSelected.png")));
				setID = "2";
				getGameData();
				backwardBtn.setEnabled(true);
				forwardBtn.setEnabled(true);
				list.clearSelection();
				resetToken();
			}
		});

		set3Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				set2Btn.setIcon(new ImageIcon(Replay.class
						.getResource("/img/setTwoUnselected.png")));
				set1Btn.setIcon(new ImageIcon(Replay.class
						.getResource("/img/setOneUnselected.png")));
				set3Btn.setIcon(new ImageIcon(Replay.class
						.getResource("/img/setThreeSelected.png")));
				setID = "3";
				getGameData();
				backwardBtn.setEnabled(true);
				forwardBtn.setEnabled(true);
				list.clearSelection();
				resetToken();
			}
		});

		homeBtn.setIcon(new ImageIcon(Replay.class.getResource("/img/home.png")));
		homeBtn.setBounds(243, 480, 35, 35);
		frame.getContentPane().add(homeBtn);
		changeWinnerBtn.setBounds(453, 111, 140, 29);
		frame.getContentPane().add(changeWinnerBtn);
		forwardBtn.setIcon(new ImageIcon(Replay.class
				.getResource("/img/forward.png")));
		forwardBtn.setBounds(373, 480, 56, 29);
		frame.getContentPane().add(forwardBtn);

		backwardBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resetToken();
				if (game.isEmpty()) {

				} else {
					list.setSelectedIndex(list.getSelectedIndex() - 1);
					list.ensureIndexIsVisible(list.getSelectedIndex() - 6);
				}
				for (int i = 0; i <= list.getSelectedIndex(); i++) {
					String[] draw = game.get(i);
					System.out.println("Index: " + i);
					System.out.println("Player : " + draw[2]);
					System.out.println("X : " + draw[0]);
					System.out.println("Y : " + draw[1]);
					setToken(draw);
				}
				if (list.getSelectedIndex() == 0) {
					backwardBtn.setEnabled(false);
				}
				if (list.getSelectedIndex() + 1 < listModel.getSize()) {
					forwardBtn.setEnabled(true);
				}
				if (list.getSelectedIndex() + 1 == listModel.getSize()) {
					setWinningToken(list.getSelectedIndex() + 1);
				}
			}
		});
		backwardBtn.setIcon(new ImageIcon(Replay.class
				.getResource("/img/backward.png")));
		backwardBtn.setBounds(89, 480, 56, 29);
		frame.getContentPane().add(backwardBtn);

		JLabel tok_1_1 = new JLabel("");
		tok_1_1.setIcon(null);
		tok_1_1.setBounds(100, 412, 35, 32);
		tok_1_1.setName("tok_1_1");
		frame.getContentPane().add(tok_1_1);

		JLabel tok_1_2 = new JLabel("");
		tok_1_2.setIcon(null);
		tok_1_2.setBounds(100, 376, 35, 32);
		tok_1_2.setName("tok_1_2");
		frame.getContentPane().add(tok_1_2);

		JLabel tok_1_3 = new JLabel("");
		tok_1_3.setIcon(null);
		tok_1_3.setBounds(100, 338, 35, 32);
		tok_1_3.setName("tok_1_3");
		frame.getContentPane().add(tok_1_3);

		JLabel tok_1_4 = new JLabel("");
		tok_1_4.setIcon(null);
		tok_1_4.setBounds(100, 302, 35, 32);
		tok_1_4.setName("tok_1_4");
		frame.getContentPane().add(tok_1_4);

		JLabel tok_1_5 = new JLabel("");
		tok_1_5.setIcon(null);
		tok_1_5.setBounds(100, 265, 35, 32);
		tok_1_5.setName("tok_1_5");
		frame.getContentPane().add(tok_1_5);

		JLabel tok_1_6 = new JLabel("");
		tok_1_6.setIcon(null);
		tok_1_6.setBounds(100, 228, 35, 32);
		tok_1_6.setName("tok_1_6");
		frame.getContentPane().add(tok_1_6);

		JLabel tok_2_1 = new JLabel("");
		tok_2_1.setIcon(null);
		tok_2_1.setBounds(148, 412, 35, 32);
		tok_2_1.setName("tok_2_1");
		frame.getContentPane().add(tok_2_1);

		JLabel tok_2_2 = new JLabel("");
		tok_2_2.setIcon(null);
		tok_2_2.setBounds(148, 376, 35, 32);
		tok_2_2.setName("tok_2_2");
		frame.getContentPane().add(tok_2_2);

		JLabel tok_2_3 = new JLabel("");
		tok_2_3.setIcon(null);
		tok_2_3.setBounds(148, 338, 35, 32);
		tok_2_3.setName("tok_2_3");
		frame.getContentPane().add(tok_2_3);

		JLabel tok_2_4 = new JLabel("");
		tok_2_4.setIcon(null);
		tok_2_4.setBounds(148, 302, 35, 32);
		tok_2_4.setName("tok_2_4");
		frame.getContentPane().add(tok_2_4);

		JLabel tok_2_5 = new JLabel("");
		tok_2_5.setIcon(null);
		tok_2_5.setBounds(148, 265, 35, 32);
		tok_2_5.setName("tok_2_5");
		frame.getContentPane().add(tok_2_5);

		JLabel tok_2_6 = new JLabel("");
		tok_2_6.setIcon(null);
		tok_2_6.setBounds(148, 228, 35, 32);
		tok_2_6.setName("tok_2_6");
		frame.getContentPane().add(tok_2_6);

		JLabel tok_3_1 = new JLabel("");
		tok_3_1.setIcon(null);
		tok_3_1.setBounds(196, 412, 35, 32);
		tok_3_1.setName("tok_3_1");
		frame.getContentPane().add(tok_3_1);

		JLabel tok_3_2 = new JLabel("");
		tok_3_2.setIcon(null);
		tok_3_2.setBounds(196, 376, 35, 32);
		tok_3_2.setName("tok_3_2");
		frame.getContentPane().add(tok_3_2);

		JLabel tok_3_3 = new JLabel("");
		tok_3_3.setIcon(null);
		tok_3_3.setBounds(196, 338, 35, 32);
		tok_3_3.setName("tok_3_3");
		frame.getContentPane().add(tok_3_3);

		JLabel tok_3_4 = new JLabel("");
		tok_3_4.setIcon(null);
		tok_3_4.setBounds(196, 302, 35, 32);
		tok_3_4.setName("tok_3_4");
		frame.getContentPane().add(tok_3_4);

		JLabel tok_3_5 = new JLabel("");
		tok_3_5.setIcon(null);
		tok_3_5.setBounds(196, 265, 35, 32);
		tok_3_5.setName("tok_3_5");
		frame.getContentPane().add(tok_3_5);

		JLabel tok_3_6 = new JLabel("");
		tok_3_6.setIcon(null);
		tok_3_6.setBounds(196, 228, 35, 32);
		tok_3_6.setName("tok_3_6");
		frame.getContentPane().add(tok_3_6);

		JLabel tok_4_1 = new JLabel("");
		tok_4_1.setIcon(null);
		tok_4_1.setBounds(243, 412, 35, 32);
		tok_4_1.setName("tok_4_1");
		frame.getContentPane().add(tok_4_1);

		JLabel tok_4_2 = new JLabel("");
		tok_4_2.setIcon(null);
		tok_4_2.setBounds(243, 376, 35, 32);
		tok_4_2.setName("tok_4_2");
		frame.getContentPane().add(tok_4_2);

		JLabel tok_4_3 = new JLabel("");
		tok_4_3.setIcon(null);
		tok_4_3.setBounds(243, 338, 35, 32);
		tok_4_3.setName("tok_4_3");
		frame.getContentPane().add(tok_4_3);

		JLabel tok_4_4 = new JLabel("");
		tok_4_4.setIcon(null);
		tok_4_4.setBounds(243, 302, 35, 32);
		tok_4_4.setName("tok_4_4");
		frame.getContentPane().add(tok_4_4);

		JLabel tok_4_5 = new JLabel("");
		tok_4_5.setIcon(null);
		tok_4_5.setBounds(243, 265, 35, 32);
		tok_4_5.setName("tok_4_5");
		frame.getContentPane().add(tok_4_5);

		JLabel tok_4_6 = new JLabel("");
		tok_4_6.setIcon(null);
		tok_4_6.setBounds(243, 228, 35, 32);
		tok_4_6.setName("tok_4_6");
		frame.getContentPane().add(tok_4_6);

		JLabel tok_5_1 = new JLabel("");
		tok_5_1.setIcon(null);
		tok_5_1.setBounds(290, 412, 35, 32);
		tok_5_1.setName("tok_5_1");
		frame.getContentPane().add(tok_5_1);

		JLabel tok_5_2 = new JLabel("");
		tok_5_2.setIcon(null);
		tok_5_2.setBounds(290, 376, 35, 32);
		tok_5_2.setName("tok_5_2");
		frame.getContentPane().add(tok_5_2);

		JLabel tok_5_3 = new JLabel("");
		tok_5_3.setIcon(null);
		tok_5_3.setBounds(290, 338, 35, 32);
		tok_5_3.setName("tok_5_3");
		frame.getContentPane().add(tok_5_3);

		JLabel tok_5_4 = new JLabel("");
		tok_5_4.setIcon(null);
		tok_5_4.setBounds(290, 302, 35, 32);
		tok_5_4.setName("tok_5_4");
		frame.getContentPane().add(tok_5_4);

		JLabel tok_5_5 = new JLabel("");
		tok_5_5.setIcon(null);
		tok_5_5.setBounds(290, 265, 35, 32);
		tok_5_5.setName("tok_5_5");
		frame.getContentPane().add(tok_5_5);

		JLabel tok_5_6 = new JLabel("");
		tok_5_6.setIcon(null);
		tok_5_6.setBounds(290, 228, 35, 32);
		tok_5_6.setName("tok_5_6");
		frame.getContentPane().add(tok_5_6);

		JLabel tok_6_1 = new JLabel("");
		tok_6_1.setIcon(null);
		tok_6_1.setBounds(337, 412, 35, 32);
		tok_6_1.setName("tok_6_1");
		frame.getContentPane().add(tok_6_1);

		JLabel tok_6_2 = new JLabel("");
		tok_6_2.setIcon(null);
		tok_6_2.setBounds(337, 376, 35, 32);
		tok_6_2.setName("tok_6_2");
		frame.getContentPane().add(tok_6_2);

		JLabel tok_6_3 = new JLabel("");
		tok_6_3.setIcon(null);
		tok_6_3.setBounds(337, 338, 35, 32);
		tok_6_3.setName("tok_6_3");
		frame.getContentPane().add(tok_6_3);

		JLabel tok_6_4 = new JLabel("");
		tok_6_4.setIcon(null);
		tok_6_4.setBounds(337, 302, 35, 32);
		tok_6_4.setName("tok_6_4");
		frame.getContentPane().add(tok_6_4);

		JLabel tok_6_5 = new JLabel("");
		tok_6_5.setIcon(null);
		tok_6_5.setBounds(337, 265, 35, 32);
		tok_6_5.setName("tok_6_5");
		frame.getContentPane().add(tok_6_5);

		JLabel tok_6_6 = new JLabel("");
		tok_6_6.setIcon(null);
		tok_6_6.setBounds(337, 228, 35, 32);
		tok_6_6.setName("tok_6_6");
		frame.getContentPane().add(tok_6_6);

		JLabel tok_7_1 = new JLabel("");
		tok_7_1.setIcon(null);
		tok_7_1.setBounds(385, 412, 35, 32);
		tok_7_1.setName("tok_7_1");
		frame.getContentPane().add(tok_7_1);

		JLabel tok_7_2 = new JLabel("");
		tok_7_2.setIcon(null);
		tok_7_2.setBounds(385, 376, 35, 32);
		tok_7_2.setName("tok_7_2");
		frame.getContentPane().add(tok_7_2);

		JLabel tok_7_3 = new JLabel("");
		tok_7_3.setIcon(null);
		tok_7_3.setBounds(385, 338, 35, 32);
		tok_7_3.setName("tok_7_3");
		frame.getContentPane().add(tok_7_3);

		JLabel tok_7_4 = new JLabel("");
		tok_7_4.setIcon(null);
		tok_7_4.setBounds(385, 302, 35, 32);
		tok_7_4.setName("tok_7_4");
		frame.getContentPane().add(tok_7_4);

		JLabel tok_7_5 = new JLabel("");
		tok_7_5.setIcon(null);
		tok_7_5.setBounds(385, 265, 35, 32);
		tok_7_5.setName("tok_7_5");
		frame.getContentPane().add(tok_7_5);

		JLabel tok_7_6 = new JLabel("");
		tok_7_6.setIcon(null);
		tok_7_6.setBounds(385, 228, 35, 32);
		tok_7_6.setName("tok_7_6");
		frame.getContentPane().add(tok_7_6);

		JLabel lblGrid = new JLabel("");
		lblGrid.setIcon(new ImageIcon(Replay.class
				.getResource("/img/gridReplay.png")));
		lblGrid.setBounds(0, 0, 720, 540);
		frame.getContentPane().add(lblGrid);

		createComponentMap();
	}

	private void createComponentMap() {
		componentMap = new HashMap<String, Component>();
		Component[] components = frame.getContentPane().getComponents();
		for (int i = 0; i < components.length; i++) {
			componentMap.put(components[i].getName(), components[i]);
		}
	}

	/**
	 * Method delivers an existing component by name which can be modified in
	 * other methods.
	 * 
	 * @param name
	 *            Parameter to identify the object within the HashMap. Every
	 *            object needs a .setName("Name") definition.
	 * @return Component as return value (for example a JLabel).
	 */
	public static Component getComponentByName(String name) {
		if (componentMap.containsKey(name)) {
			return (Component) componentMap.get(name);
		} else
			return null;
	}

	/**
	 * Method sets the ID of the match thats is displayed on the UI.
	 * 
	 * @param gameParameter
	 *            Match-ID
	 */
	public void setGame(String gameParameter) {
		gameID = gameParameter;
		prepareUi();
	}
}
