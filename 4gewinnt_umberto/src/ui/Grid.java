package ui;

import home.Administration;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import models.Draw;
import models.Token;
import javax.swing.SwingConstants;

/**
 * The purpose of Grid.java is to display the graphical user interface for
 * 'connect 4' matches. Every user interaction during the match is triggered by
 * Grid.java.
 * 
 * @author Bender
 * 
 */
public class Grid {

	private static JFrame frame;
	private static HashMap<String, Component> componentMap;
	static Point mouseDownCompCoords;
	private static boolean startFlag;
	private int timeCounter = 0;
	private static int setCounter = 0;
	private boolean timeKiller = false;
	private JLabel lblOpponentName;
	public Thread startGame;
	UserInterface userInterface;

	/**
	 * Constructor to create a Grid() object with all components.
	 * 
	 * @param userInterface
	 *            Transfer parameter to set the current Grid as UserInterface.
	 */
	public Grid(UserInterface userInterface) {
		this.userInterface = userInterface;
		initialize();
		frame.setVisible(true);
		resetToken();
	}

	/**
	 * startGame() initializes the game process by starting a timer, increasing
	 * the setCounter and triggering the startGame-Method within
	 * Administration.java.
	 * 
	 */
	public void startGame() {
		startTimer();
		try {
			setSetCount();
			userInterface.getAdministration().startGame();
		} catch (IOException e) {
			System.err
					.println("Grid: Error during triggering Administration.loop(); by Grid.startGame();");
			e.printStackTrace();
		}
	}

	private void setSetCount() {
		JLabel setLabel = (JLabel) getComponentByName("lblSet");
		setCounter++;
		setLabel.setText("Satz " + setCounter);
	}

	/**
	 * Method returns value of current set to support database operations.
	 * 
	 * @return The return value is an integer value from 0 to 3 (0 = no current
	 *         set, 1-3 = setID).
	 */
	public static int getSetCount() {
		return setCounter;
	}

	/**
	 * changeOpponentName(String newOpponentName) changes the
	 * lblOpponentName.text() and displays the new name on the user interface.
	 * 
	 * @param newOpponentName
	 *            Transfer parameter (String) which will be displayed on the UI.
	 */
	public void changeOpponentName(String newOpponentName) {
		lblOpponentName.setText(newOpponentName);
	}

	/**
	 * Initiates Dialog to make sure that the user really wants to cancel the
	 * current match.
	 * 
	 */
	public void cancelGame() {
		if (startFlag == true) {
			Object[] options = { "Nein", "Ja" };
			int n = JOptionPane.showOptionDialog(frame,
					"M\u00f6chten Sie das laufende Spiel wirklich abbrechen?",
					"Warnung", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

			if (n == 1) {
				setStartFlag(false);
				stopTimer();
				resetToken();
				startGame.stop();
				userInterface.getAdministration().resetGame();
			}
		}
	}

	/**
	 * Method to set token on grid by displaying the applicable icon.
	 * 
	 * @param draw
	 *            Container/Object with all necessary information to set a token
	 *            on the grid (Including x-coordinate, y-coordinate and
	 *            playerID).
	 * @return Boolean flag to confirm that the token has been successfully set.
	 */
	public static boolean setToken(Draw draw) {

		JLabel token = (JLabel) getComponentByName("tok_"
				+ (draw.getToken().getxCoordinate() + 1) + "_"
				+ (draw.getToken().getyCoordinate() + 1));

		switch (draw.getPlayer()) {
		default:
			System.err.println("Grid: Übergebene SpielerID != 1 oder 2");
			return false;
		case 1:
			token.setIcon(new ImageIcon(Grid.class.getResource("/img/red.png")));
			return true;

		case 2:
			token.setIcon(new ImageIcon(Grid.class.getResource("/img/yel.png")));
			return true;
		}
	}

	/**
	 * Method to set a green token on the UI which indicates a winning streak (4
	 * in a row).
	 * 
	 * @param winningToken
	 *            Container/Object with all necessary information to set a token
	 *            on the grid (Including x-coordinate, y-coordinate and
	 *            playerID).
	 */
	public void setWinningToken(Token[] winningToken) {
		for (int i = 0; i < winningToken.length; i++) {
			JLabel token = (JLabel) getComponentByName("tok_"
					+ (winningToken[i].getxCoordinate() + 1) + "_"
					+ (winningToken[i].getyCoordinate() + 1));
			token.setIcon(new ImageIcon(Grid.class.getResource("/img/gre.png")));
		}
	}

	/**
	 * Method to get the value of the startFlag which indicates if a match is
	 * currently running.
	 * 
	 * @return Boolean value which contains the flag.
	 */
	public boolean getStartFlag() {
		return startFlag;
	}

	/**
	 * Method to confirm that a match is currently running.
	 * 
	 */
	private void setStartFlag(boolean flag) {
		startFlag = flag;
	}

	/**
	 * Method start timer which is displayed on the UI. Timer increases by 1
	 * every second.
	 * 
	 */
	public void startTimer() {
		final JLabel timerLabel = (JLabel) getComponentByName("lblTimer");
		timeCounter = 0;
		timeKiller = false;
		final Timer timer = new Timer();
		TimerTask task = new TimerTask() {

			public void run() {
				if (timeKiller == true) {
					timer.cancel();
					timer.purge();
				} else {
					int min = timeCounter / 60;
					int sec = timeCounter % 60;

					NumberFormat nf = NumberFormat.getIntegerInstance();
					nf.setMinimumIntegerDigits(2);
					nf.setMaximumIntegerDigits(2);
					nf.setGroupingUsed(false);

					timerLabel.setText(nf.format(min) + ":" + nf.format(sec));
					timeCounter++;
				}

			}
		};
		timer.schedule(task, 0, 1000);
	}

	private void stopTimer() {
		timeKiller = true;
	}

	private void resetToken() {
		for (int i = 1; i <= 7; i++) {
			for (int j = 1; j <= 6; j++) {
				JLabel token = (JLabel) getComponentByName("tok_" + i + "_" + j);
				token.setIcon(null);
			}
		}
	}

	/**
	 * Initialization of all UI Elements
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(300, 200, 720, 540);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setUndecorated(true);
		mouseDownCompCoords = null;
		frame.addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {
				mouseDownCompCoords = null;
			}

			public void mousePressed(MouseEvent e) {
				mouseDownCompCoords = e.getPoint();
			}

			public void mouseExited(MouseEvent e) {
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseClicked(MouseEvent e) {
			}
		});

		frame.addMouseMotionListener(new MouseMotionListener() {
			public void mouseMoved(MouseEvent e) {
			}

			public void mouseDragged(MouseEvent e) {
				Point currCoords = e.getLocationOnScreen();
				frame.setLocation(currCoords.x - mouseDownCompCoords.x,
						currCoords.y - mouseDownCompCoords.y);
			}
		});

		JButton homeBtn = new JButton("");
		homeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Start.resetList();
				new Administration();
				if (frame.isDisplayable()) {
					frame.dispose();
				}
			}
		});

		JLabel playerNamelbl = new JLabel("Umberto");
		playerNamelbl.setHorizontalAlignment(SwingConstants.LEFT);
		playerNamelbl.setForeground(Color.WHITE);
		playerNamelbl.setFont(new Font("Lucida Grande", Font.BOLD, 18));
		playerNamelbl.setBounds(124, 44, 133, 35);
		frame.getContentPane().add(playerNamelbl);
		homeBtn.setIcon(new ImageIcon(Grid.class.getResource("/img/home.png")));
		homeBtn.setBounds(342, 472, 35, 35);
		frame.getContentPane().add(homeBtn);

		final JLabel tok_1_1 = new JLabel("");
		tok_1_1.setIcon(null);
		tok_1_1.setBounds(150, 401, 44, 40);
		tok_1_1.setName("tok_1_1");
		frame.getContentPane().add(tok_1_1);

		JLabel tok_1_2 = new JLabel("");
		tok_1_2.setIcon(null);
		tok_1_2.setBounds(150, 352, 44, 40);
		tok_1_2.setName("tok_1_2");
		frame.getContentPane().add(tok_1_2);

		JLabel tok_1_3 = new JLabel("");
		tok_1_3.setIcon(null);
		tok_1_3.setBounds(150, 304, 44, 40);
		tok_1_3.setName("tok_1_3");
		frame.getContentPane().add(tok_1_3);

		JLabel tok_1_4 = new JLabel("");
		tok_1_4.setIcon(null);
		tok_1_4.setBounds(150, 256, 44, 40);
		tok_1_4.setName("tok_1_4");
		frame.getContentPane().add(tok_1_4);

		JLabel tok_1_5 = new JLabel("");
		tok_1_5.setIcon(null);
		tok_1_5.setBounds(150, 206, 44, 40);
		tok_1_5.setName("tok_1_5");
		frame.getContentPane().add(tok_1_5);

		JLabel tok_1_6 = new JLabel("");
		tok_1_6.setIcon(null);
		tok_1_6.setBounds(150, 157, 44, 40);
		tok_1_6.setName("tok_1_6");
		frame.getContentPane().add(tok_1_6);

		JLabel tok_2_1 = new JLabel("");
		tok_2_1.setIcon(null);
		tok_2_1.setBounds(213, 401, 44, 40);
		tok_2_1.setName("tok_2_1");
		frame.getContentPane().add(tok_2_1);

		JLabel tok_2_2 = new JLabel("");
		tok_2_2.setIcon(null);
		tok_2_2.setBounds(213, 352, 44, 40);
		tok_2_2.setName("tok_2_2");
		frame.getContentPane().add(tok_2_2);

		JLabel tok_2_3 = new JLabel("");
		tok_2_3.setIcon(null);
		tok_2_3.setBounds(213, 304, 44, 40);
		tok_2_3.setName("tok_2_3");
		frame.getContentPane().add(tok_2_3);

		JLabel tok_2_4 = new JLabel("");
		tok_2_4.setIcon(null);
		tok_2_4.setBounds(213, 256, 44, 40);
		tok_2_4.setName("tok_2_4");
		frame.getContentPane().add(tok_2_4);

		JLabel tok_2_5 = new JLabel("");
		tok_2_5.setIcon(null);
		tok_2_5.setBounds(213, 206, 44, 40);
		tok_2_5.setName("tok_2_5");
		frame.getContentPane().add(tok_2_5);

		JLabel tok_2_6 = new JLabel("");
		tok_2_6.setIcon(null);
		tok_2_6.setBounds(213, 157, 44, 40);
		tok_2_6.setName("tok_2_6");
		frame.getContentPane().add(tok_2_6);

		JLabel tok_3_1 = new JLabel("");
		tok_3_1.setIcon(null);
		tok_3_1.setBounds(276, 401, 44, 40);
		tok_3_1.setName("tok_3_1");
		frame.getContentPane().add(tok_3_1);

		JLabel tok_3_2 = new JLabel("");
		tok_3_2.setIcon(null);
		tok_3_2.setBounds(276, 352, 44, 40);
		tok_3_2.setName("tok_3_2");
		frame.getContentPane().add(tok_3_2);

		JLabel tok_3_3 = new JLabel("");
		tok_3_3.setIcon(null);
		tok_3_3.setBounds(276, 304, 44, 40);
		tok_3_3.setName("tok_3_3");
		frame.getContentPane().add(tok_3_3);

		JLabel tok_3_4 = new JLabel("");
		tok_3_4.setIcon(null);
		tok_3_4.setBounds(276, 256, 44, 40);
		tok_3_4.setName("tok_3_4");
		frame.getContentPane().add(tok_3_4);

		JLabel tok_3_5 = new JLabel("");
		tok_3_5.setIcon(null);
		tok_3_5.setBounds(276, 206, 44, 40);
		tok_3_5.setName("tok_3_5");
		frame.getContentPane().add(tok_3_5);

		JLabel tok_3_6 = new JLabel("");
		tok_3_6.setIcon(null);
		tok_3_6.setBounds(276, 157, 44, 40);
		tok_3_6.setName("tok_3_6");
		frame.getContentPane().add(tok_3_6);

		JLabel tok_4_1 = new JLabel("");
		tok_4_1.setIcon(null);
		tok_4_1.setBounds(339, 401, 44, 40);
		tok_4_1.setName("tok_4_1");
		frame.getContentPane().add(tok_4_1);

		JLabel tok_4_2 = new JLabel("");
		tok_4_2.setIcon(null);
		tok_4_2.setBounds(339, 352, 44, 40);
		tok_4_2.setName("tok_4_2");
		frame.getContentPane().add(tok_4_2);

		JLabel tok_4_3 = new JLabel("");
		tok_4_3.setIcon(null);
		tok_4_3.setBounds(339, 304, 44, 40);
		tok_4_3.setName("tok_4_3");
		frame.getContentPane().add(tok_4_3);

		JLabel tok_4_4 = new JLabel("");
		tok_4_4.setIcon(null);
		tok_4_4.setBounds(339, 256, 44, 40);
		tok_4_4.setName("tok_4_4");
		frame.getContentPane().add(tok_4_4);

		JLabel tok_4_5 = new JLabel("");
		tok_4_5.setIcon(null);
		tok_4_5.setBounds(339, 206, 44, 40);
		tok_4_5.setName("tok_4_5");
		frame.getContentPane().add(tok_4_5);

		JLabel tok_4_6 = new JLabel("");
		tok_4_6.setIcon(null);
		tok_4_6.setBounds(339, 157, 44, 40);
		tok_4_6.setName("tok_4_6");
		frame.getContentPane().add(tok_4_6);

		JLabel tok_5_1 = new JLabel("");
		tok_5_1.setIcon(null);
		tok_5_1.setBounds(402, 401, 44, 40);
		tok_5_1.setName("tok_5_1");
		frame.getContentPane().add(tok_5_1);

		JLabel tok_5_2 = new JLabel("");
		tok_5_2.setIcon(null);
		tok_5_2.setBounds(402, 352, 44, 40);
		tok_5_2.setName("tok_5_2");
		frame.getContentPane().add(tok_5_2);

		JLabel tok_5_3 = new JLabel("");
		tok_5_3.setIcon(null);
		tok_5_3.setBounds(402, 304, 44, 40);
		tok_5_3.setName("tok_5_3");
		frame.getContentPane().add(tok_5_3);

		JLabel tok_5_4 = new JLabel("");
		tok_5_4.setIcon(null);
		tok_5_4.setBounds(402, 256, 44, 40);
		tok_5_4.setName("tok_5_4");
		frame.getContentPane().add(tok_5_4);

		JLabel tok_5_5 = new JLabel("");
		tok_5_5.setIcon(null);
		tok_5_5.setBounds(402, 206, 44, 40);
		tok_5_5.setName("tok_5_5");
		frame.getContentPane().add(tok_5_5);

		JLabel tok_5_6 = new JLabel("");
		tok_5_6.setIcon(null);
		tok_5_6.setBounds(402, 157, 44, 40);
		tok_5_6.setName("tok_5_6");
		frame.getContentPane().add(tok_5_6);

		JLabel tok_6_1 = new JLabel("");
		tok_6_1.setIcon(null);
		tok_6_1.setBounds(465, 401, 44, 40);
		tok_6_1.setName("tok_6_1");
		frame.getContentPane().add(tok_6_1);

		JLabel tok_6_2 = new JLabel("");
		tok_6_2.setIcon(null);
		tok_6_2.setBounds(465, 352, 44, 40);
		tok_6_2.setName("tok_6_2");
		frame.getContentPane().add(tok_6_2);

		JLabel tok_6_3 = new JLabel("");
		tok_6_3.setIcon(null);
		tok_6_3.setBounds(465, 304, 44, 40);
		tok_6_3.setName("tok_6_3");
		frame.getContentPane().add(tok_6_3);

		JLabel tok_6_4 = new JLabel("");
		tok_6_4.setIcon(null);
		tok_6_4.setBounds(465, 256, 44, 40);
		tok_6_4.setName("tok_6_4");
		frame.getContentPane().add(tok_6_4);

		JLabel tok_6_5 = new JLabel("");
		tok_6_5.setIcon(null);
		tok_6_5.setBounds(465, 206, 44, 40);
		tok_6_5.setName("tok_6_5");
		frame.getContentPane().add(tok_6_5);

		JLabel tok_6_6 = new JLabel("");
		tok_6_6.setIcon(null);
		tok_6_6.setBounds(465, 157, 44, 40);
		tok_6_6.setName("tok_6_6");
		frame.getContentPane().add(tok_6_6);

		JLabel tok_7_1 = new JLabel("");
		tok_7_1.setIcon(null);
		tok_7_1.setBounds(528, 401, 44, 40);
		tok_7_1.setName("tok_7_1");
		frame.getContentPane().add(tok_7_1);

		JLabel tok_7_2 = new JLabel("");
		tok_7_2.setIcon(null);
		tok_7_2.setBounds(528, 352, 44, 40);
		tok_7_2.setName("tok_7_2");
		frame.getContentPane().add(tok_7_2);

		JLabel tok_7_3 = new JLabel("");
		tok_7_3.setIcon(null);
		tok_7_3.setBounds(528, 304, 44, 40);
		tok_7_3.setName("tok_7_3");
		frame.getContentPane().add(tok_7_3);

		JLabel tok_7_4 = new JLabel("");
		tok_7_4.setIcon(null);
		tok_7_4.setBounds(528, 256, 44, 40);
		tok_7_4.setName("tok_7_4");
		frame.getContentPane().add(tok_7_4);

		JLabel tok_7_5 = new JLabel("");
		tok_7_5.setIcon(null);
		tok_7_5.setBounds(528, 206, 44, 40);
		tok_7_5.setName("tok_7_5");
		frame.getContentPane().add(tok_7_5);

		JLabel tok_7_6 = new JLabel("");
		tok_7_6.setIcon(null);
		tok_7_6.setBounds(528, 157, 44, 40);
		tok_7_6.setName("tok_7_6");
		frame.getContentPane().add(tok_7_6);

		lblOpponentName = new JLabel("Gegnername");
		lblOpponentName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblOpponentName.setFont(new Font("Lucida Grande", Font.BOLD, 18));
		lblOpponentName.setForeground(Color.WHITE);
		lblOpponentName.setBounds(465, 44, 133, 35);
		frame.getContentPane().add(lblOpponentName);

		final JButton btnStartGame = new JButton("Spiel Starten");
		btnStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setStartFlag(true);

				Runnable startGameThread = new Runnable() {
					public void run() {
						// Execute time-consuming task
						startGame();
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								// Update UI
							}
						});
					}
				};
				startGame = new Thread(startGameThread);
				startGame.start();
				btnStartGame.setEnabled(false);
			}
		});
		btnStartGame.setBounds(57, 472, 141, 29);
		frame.getContentPane().add(btnStartGame);

		final JButton btnCancelGame = new JButton("Satz Beenden");
		btnCancelGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelGame();
				btnStartGame.setEnabled(true);
			}
		});
		btnCancelGame.setBounds(528, 472, 141, 29);
		frame.getContentPane().add(btnCancelGame);

		JLabel lblTimer = new JLabel("00:00");
		lblTimer.setHorizontalAlignment(SwingConstants.CENTER);
		lblTimer.setForeground(Color.WHITE);
		lblTimer.setFont(new Font("Lucida Grande", Font.BOLD, 15));
		lblTimer.setBounds(323, 74, 73, 16);
		lblTimer.setName("lblTimer");
		frame.getContentPane().add(lblTimer);

		JLabel lblSet = new JLabel("Satz");
		lblSet.setHorizontalAlignment(SwingConstants.CENTER);
		lblSet.setForeground(Color.WHITE);
		lblSet.setFont(new Font("Lucida Grande", Font.BOLD, 15));
		lblSet.setBounds(329, 27, 61, 35);
		lblSet.setName("lblSet");
		frame.getContentPane().add(lblSet);

		JLabel lblGrid = new JLabel("");
		lblGrid.setIcon(new ImageIcon(Grid.class.getResource("/img/grid.png")));
		lblGrid.setBounds(0, 0, 720, 540);
		frame.getContentPane().add(lblGrid);

		createComponentMap();
	}

	private void createComponentMap() {
		componentMap = new HashMap<String, Component>();
		Component[] components = frame.getContentPane().getComponents();
		for (int i = 0; i < components.length; i++) {
			componentMap.put(components[i].getName(), components[i]);
		}
	}

	/**
	 * Method delivers an existing component by name which can be modified in
	 * other methods.
	 * 
	 * @param name
	 *            Parameter to identify the object within the HashMap. Every
	 *            object needs a .setName("Name") definition.
	 * @return Component as return value (for example a JLabel).
	 */
	public static Component getComponentByName(String name) {
		if (componentMap.containsKey(name)) {
			return (Component) componentMap.get(name);
		} else
			return null;
	}
}
