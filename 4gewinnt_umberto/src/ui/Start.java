package ui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.MatteBorder;

import db.Database;

/**
 * Start.java displays the start screen to select different game options like
 * replay and new game.
 * 
 * @author Bender
 *
 */
public class Start {

	public static Grid grid;
	private UserInterface userInterface;
	private JFrame frame;
	static Point mouseDownCompCoords;
	private static DefaultListModel listModel = new DefaultListModel();
	private ArrayList<String[]> games;

	/**
	 * Constructor to create a new star window.
	 * 
	 * @param userInterface
	 */
	public Start(UserInterface userInterface) {
		this.userInterface = userInterface;
		initialize();
		getGames();
		this.frame.setVisible(true);
		startMovingToken();
	}

	/**
	 * Method creates a connection to the database and receives all existing
	 * games.
	 * 
	 */
	public void getGames() {

		Database db = new Database();
		games = db.getGameDataForStartScreen();

		for (String[] entry : games) {
			listModel.addElement("Umberto vs. " + entry[1]
					+ " -  Spieldatum:  " + entry[2] + " - Score: " + entry[3]);
		}

	}

	/**
	 * Creates the animated tokens floating in the background of the start
	 * window.
	 * 
	 */
	public void startMovingToken() {
		final int cloneCounter = 10;

		final JLabel labels[] = new JLabel[cloneCounter];
		for (int i = 0; i < cloneCounter; i++) {
			int randomColor = 1 + (int) (Math.random() * ((2 - 1) + 1));
			int randomX = -20 + (int) (Math.random() * ((660 - -20) + 1));
			int randomY = -20 + (int) (Math.random() * ((500 - -20) + 1));

			labels[i] = new JLabel("moveLabel" + cloneCounter);
			labels[i].setBounds(randomX, randomY, 44, 40);
			switch (randomColor) {
			case 1:
				labels[i].setIcon(new ImageIcon(Grid.class
						.getResource("/img/red.png")));
				break;
			case 2:
				labels[i].setIcon(new ImageIcon(Grid.class
						.getResource("/img/yel.png")));
				break;
			default:
				break;
			}

			frame.getContentPane().add(labels[i]);
		}

		final Timer timer = new Timer();
		TimerTask task = new TimerTask() {

			public void run() {
				for (int j = 0; j < cloneCounter; j++) {

					int x = labels[j].getX();
					int y = labels[j].getY();
					if (y > 500) {
						x = -20 + (int) (Math.random() * ((660 - -20) + 1));
						y = -50;
					}
					labels[j].setLocation(x, y + 1);
				}
			}
		};
		timer.schedule(task, 0, 15);
	}

	/**
	 * Resets all selections form the current list.
	 * 
	 */
	public static void resetList() {
		listModel.clear();
	}

	/**
	 * Initialization of all UI-elements.
	 * 
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(420, 200, 610, 500);
		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mouseDownCompCoords = null;
		frame.addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {
				mouseDownCompCoords = null;
			}

			public void mousePressed(MouseEvent e) {
				mouseDownCompCoords = e.getPoint();
			}

			public void mouseExited(MouseEvent e) {
			}

			public void mouseEntered(MouseEvent e) {
			}

			public void mouseClicked(MouseEvent e) {
			}
		});

		frame.addMouseMotionListener(new MouseMotionListener() {
			public void mouseMoved(MouseEvent e) {
			}

			public void mouseDragged(MouseEvent e) {
				Point currCoords = e.getLocationOnScreen();
				frame.setLocation(currCoords.x - mouseDownCompCoords.x,
						currCoords.y - mouseDownCompCoords.y);
			}
		});

		JButton btnExit = new JButton("");
		btnExit.setBounds(586, 6, 18, 14);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setIcon(new ImageIcon(Start.class.getResource("/img/exit.png")));
		btnExit.setBorder(BorderFactory.createEmptyBorder());
		btnExit.setContentAreaFilled(false);
		btnExit.setCursor(new Cursor(Cursor.HAND_CURSOR));

		JLabel label = new JLabel("");
		label.setBounds(6, 23, 598, 232);
		label.setIcon(new ImageIcon(Start.class.getResource("/img/title.png")));

		JButton btnNewGame = new JButton("Neues Spiel");
		btnNewGame.setBounds(207, 254, 196, 38);
		btnNewGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField opponentName = new JTextField();
				final JTextField path = new JTextField();
				JRadioButton playerX = new JRadioButton("x");
				JRadioButton playerO = new JRadioButton("o");
				ButtonGroup playerSelection = new ButtonGroup();
				playerSelection.add(playerX);
				playerSelection.add(playerO);
				JButton btnChoosePath = new JButton("Pfad ausw\u00e4hlen...");
				btnChoosePath.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						JFileChooser chooser = new JFileChooser();
						chooser.setCurrentDirectory(new java.io.File("."));
						chooser.setDialogTitle("Pfad ausw\u00e4hlen");
						chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
						chooser.setAcceptAllFileFilterUsed(false);

						if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
							path.setText(chooser.getSelectedFile().toString()
									+ System.getProperty("file.separator"));
						}
					}
				});

				Object[] message = { "Gegnername:", opponentName,
						"Umberto ist welcher Spieler?", playerX, playerO,
						"Datei-Pfad:", path, btnChoosePath };
				Object[] options = { "Starten", "Abbrechen" };

				JOptionPane inputMask = new JOptionPane(message,
						JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION,
						null, options, options[0]);
				inputMask.createDialog(null, "Spieleingaben").setVisible(true);

				if (inputMask.getValue() == "Starten") {
					// get all Field data
					String opponentNameText = opponentName.getText();
					Boolean selection = playerX.isSelected()
							|| playerO.isSelected();
					String pathValue = path.getText();

					// Check values of fields
					Boolean validInput = checkInputFields(opponentNameText,
							selection, pathValue);

					if (validInput) {
						ButtonModel selectedModel = playerSelection
								.getSelection();
						String serverPlayerName = "";
						if (playerX.getModel().equals(selectedModel)) {
							serverPlayerName = "x";
						} else {
							serverPlayerName = "o";
						}
						System.out.println("UserInterface: serverPlayerName= "
								+ serverPlayerName);

						System.out.println("Start:  User Interface: "
								+ userInterface.toString());
						System.out
								.println("Start:  User Interface.administration "
										+ userInterface.getAdministration()
												.toString());
						userInterface.getAdministration()
								.loadConfigurationDataFromGuiForNewGame(
										pathValue, opponentNameText,
										serverPlayerName);
						userInterface.initializeGrid(opponentName.getText());
						if (frame.isDisplayable()) {
							frame.dispose();
						}
					} else {
						System.out
								.println("Fehler bei der Überprüfung der Eingabemaske");
					}

				}
			}
		});

		final JList list = new JList(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setName("list");
		DefaultListCellRenderer renderer = (DefaultListCellRenderer) list
				.getCellRenderer();
		renderer.setHorizontalAlignment(JLabel.CENTER);

		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBounds(84, 345, 442, 147);
		scrollPane.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0,
				0, 0)));

		frame.getContentPane().add(scrollPane);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(label);
		frame.getContentPane().add(btnExit);
		frame.getContentPane().add(btnNewGame);

		JButton btnLoadGame = new JButton("Spiel Laden");
		btnLoadGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (list.getSelectedIndex() != -1) {
					if (frame.isDisplayable()) {
						frame.dispose();
					}
					System.out.println(games.get(list.getSelectedIndex())[0]);
					userInterface.initializeReplay(games.get(list
							.getSelectedIndex())[0]);

				} else {
					System.err.println("Keine Listenauswahl getroffen!");
				}
			}
		});
		btnLoadGame.setBounds(207, 295, 196, 38);
		frame.getContentPane().add(btnLoadGame);

	}

	/**
	 * Checks if all input fields are valid
	 * 
	 * @param opponent
	 *            The text of the field 'opponent'
	 * @param selected
	 *            Boolean if at one radio box of ServerPlayerName is selected
	 * @param path
	 *            The text of the field 'server path'
	 * @return Boolean if all fields have a valid input
	 */
	private Boolean checkInputFields(String opponent, Boolean selected,
			String path) {

		if (opponent.length() <= 3) {
			return false;
		}

		if (!selected) {
			return false;
		}

		return true;

	}
}
