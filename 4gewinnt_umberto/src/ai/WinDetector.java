package ai;

import models.PlayingField;
import models.Token;

/**
 * This class uses a playingfield as input and discover whether a winner is
 * found in this field.
 * 
 * @author Stefan Weimann
 */
public class WinDetector
{
	private PlayingField playGround;
	private int[][] field;
	private int height;
	private int width;
	private int winner = 0;
	private int player = 0;
	private int counter = 0;
	private int diagonalcol;
	private int diagonalrow;
	private Token[] winnertoken;
	private Token currentToken;

	/**
	 * Constructs an instance of the WinDetector-class
	 */
	public WinDetector() {
		winnertoken = new Token[4];
	}

	/**
	 * This method is required to detect a winner. It needs a playingfield as
	 * input and loads its properties.
	 * 
	 * @param playingField
	 *            This field has to be set after each round to find a winner on
	 *            it.
	 */
	public void setPlayGround(PlayingField playingField) {
		this.playGround = playingField;
		field = playingField.getField();
		width = playingField.getWIDTH();
		height = playingField.getHEIGHT();
	}

	/**
	 * This method returns a winner.
	 * 
	 * @return If nobody has won, this method will return 0.
	 */
	public int getWinner() {
		return winner;
	}

	/**
	 * This method returns the 4 token which describes the winning combination
	 * 
	 * @return If nobody has won, this method will return null.
	 */
	public Token[] getWinnertoken() {
		return winnertoken;
	}

	/**
	 * This method starts the detection.
	 * 
	 * @return Returns a winner. If nobody has won, this method will return 0.
	 */
	public int detectWinner() {
		detectColumnWinPossibilites();
		detectRowWinPossibilites();
		detectDiagonalWinPossibilities();
		return winner;
	}

	private void detectColumnWinPossibilites() {
		alterThroughColumns();
	}

	private void alterThroughColumns() {
		for (int column = 0; column < width; column++) {
			resetIntegerValues();
			for (int row = 0; row < height; row++) {
				checkVariations(column, row);
				if (winner != 0) {
					return;
				}
			}
		}
	}

	private void resetIntegerValues() {
		player = 0;
		counter = 0;
	}

	private void checkVariations(int col, int row) {
		if (player == field[col][row] && field[col][row] != 0) {
			counter++;
			fillWinFields(col, row, counter);
		}
		if (counter == 4) {
			winner = player;
			return;
		}
		if (player != field[col][row] && field[col][row] != 0) {
			player = field[col][row];
			counter = 1;
			fillWinFields(col, row, counter);
		}
		if (field[col][row] == 0) {
			resetIntegerValues();
		}
	}

	private void fillWinFields(int col, int row, int index) {
		currentToken = new Token();
		currentToken.setxCoordinate(col);
		currentToken.setyCoordinate(row);
		winnertoken[index - 1] = currentToken;
	}

	private void detectRowWinPossibilites() {
		if (winner != 0) {
			return;
		}
		alterThroughRows();
	}

	private void alterThroughRows() {
		for (int row = 0; row < height; row++) {
			resetIntegerValues();
			for (int column = 0; column < width; column++) {
				checkVariations(column, row);
				if (winner != 0) {
					return;
				}
			}
		}
	}

	private void detectDiagonalWinPossibilities() {
		if (winner != 0) {
			return;
		}
		alterThroughField();
	}

	private void alterThroughField() {
		for (int row = 0; row < height; row++) {
			for (int column = 0; column < width; column++) {
				resetIntegerValues();
				player = field[column][row];
				if (player != 0) {
					counter = 1;
					fillWinFields(column, row, counter);
					checkUpperDiagonalToken(column, row);
				}
				if (winner != 0) {
					return;
				}
			}
		}
	}

	private void checkUpperDiagonalToken(int x, int y) {
		resetDiagonalCoordinates(x, y);
		while (identifyPlayersToken(loadDiagonalToken(diagonalcol - 1,
				diagonalrow + 1))) {
			if (counter == 4) {
				winner = player;
				return;
			}
			diagonalcol--;
			diagonalrow++;
		}

		counter = 1;
		resetDiagonalCoordinates(x, y);
		while (identifyPlayersToken(loadDiagonalToken(diagonalcol + 1,
				diagonalrow + 1))) {
			if (counter == 4) {
				winner = player;
				return;
			}
			diagonalcol++;
			diagonalrow++;
		}
	}

	private void resetDiagonalCoordinates(int col, int row) {
		diagonalcol = col;
		diagonalrow = row;
	}

	private Token loadDiagonalToken(int col, int row) {
		if (checkIfTokenIsWithinField(col, row)) {
			currentToken = new Token();
			currentToken.setxCoordinate(col);
			currentToken.setyCoordinate(row);
			return currentToken;
		}
		return null;
	}

	private boolean checkIfTokenIsWithinField(int col, int row) {
		if (col < 0 || col >= playGround.getWIDTH() || row < 0
				|| row >= playGround.getHEIGHT()) {
			return false;
		}
		return true;
	}

	private boolean identifyPlayersToken(Token token) {
		if (token != null) {
			if (field[token.getxCoordinate()][token.getyCoordinate()] == player) {
				counter++;
				fillWinFields(token.getxCoordinate(), token.getyCoordinate(),
						counter);
				return true;
			}
		}
		return false;
	}
}
