package ai;

import models.PlayingField;

/**
 * Constructs a GameIntelligence which is required for the calculation for the
 * next move.
 * 
 * @author Stefan Weimann
 */
public class GameIntelligence
{
	private PlayingField field;
	private int simulationQuantity;
	private int winner;
	private WinDetector winDetector; 

	/**
	 * Constructor of the class GameIntelligence. It will initialize the
	 * GameIntelligence with a default number of simulation value of 5000
	 */
	public GameIntelligence() {
		this(1000);
	}

	/**
	 * @return the actual winner. If nobody has won the return value is 0.
	 */
	public int getWinner() {
		return winner;
	}

	/**
	 * Constructor of the class GameIntelligence. Here you can define how many
	 * simulations will be executed
	 * 
	 * @param numberOfSimulations
	 *            for each possible column, you can the define the number of
	 *            Simulations
	 */
	public GameIntelligence(int numberOfSimulations) {
		simulationQuantity = numberOfSimulations;
		winDetector = new WinDetector();
	}

	/**
	 * Setter for a PlayingField
	 * 
	 * @param field
	 *            places the current Playingfield
	 */
	public void setField(PlayingField field) {
		this.field = field;
	}

	/**
	 * This method initiates the Simulator and calculate the next move for the
	 * game.
	 * 
	 * @return the column for next move. Possible return values are from 0 to 6.
	 */
	public int calculateNextMove() {
		long zstVorher;
		long zstNachher;

		zstVorher = System.currentTimeMillis();
		Simulator simulator = new Simulator(field);
		simulator.simulate(simulationQuantity);
		zstNachher = System.currentTimeMillis();
		System.out.println("GameIntelligence: Next Move: "
				+ simulator.getColumnForNextMove());
		System.out.println("GameIntelligence: Zeit benötigt: "
				+ ((zstNachher - zstVorher)) + " ms");
		return simulator.getColumnForNextMove();
	}
	
	/**
	 * @param playingField The current PlayingField to calculate the winner.
	 * @return Returns 0 if no winner is detected yet, 1 if Umberto is winner and 2 if the enemy is the winner.
	 */
	public int getWinner(PlayingField playingField)
	{
		winDetector.setPlayGround(playingField);
		winDetector.detectWinner();
		return winDetector.getWinner();
	}
	
	public WinDetector getWinDetector()
	{
		return winDetector;
	}
}
