package ai;

/**
 * This class tries to interpret the result of all simulations.
 * 
 * @author Stefan Weimann
 */
public class SimulationInterpreter
{
	private int[][] simulationResults;
	private int[] simulationPercentages;
	private int nextMove;

	/**
	 * Constructs an instance of The Interpreter-class
	 */
	public SimulationInterpreter() {
		simulationResults = new int[7][3];
		simulationPercentages = new int[7];
	}

	/**
	 * @return After this class evaluated the simulations, you can get the next
	 *         move with this method
	 */
	public int getNextMove() {
		return nextMove;
	}

	/**
	 * With this method you create the simulation base. For a good simulation
	 * result you have to add as much simulations as you can.
	 * 
	 * @param winner
	 *            Sets the winner of the actual simulation
	 * @param column
	 *            Sets the column which is a possible next move.
	 */
	public void addSimulationResult(int winner, int column) {
		simulationResults[column][winner]++;
	}

	/**
	 * This method starts the interpretation of all simulation results.
	 * 
	 * @param simulationsPerColumn
	 *            You need to set the maximum of the simulations per possible
	 *            column.
	 */
	public void interpretSimulation(int simulationsPerColumn) {
		for (int i = 0; i < 7; i++) {
			int roundedColumnValue = (int) Math
					.round((double) simulationResults[i][1] * 100
							/ (double) simulationsPerColumn);
			simulationPercentages[i] = roundedColumnValue;
		}

		int currentPercentage = 0;

		for (int i = 0; i < 7; i++) {
			if (currentPercentage < simulationPercentages[i]) {
				currentPercentage = simulationPercentages[i];
				nextMove = i;
			}
		}
	}
}
