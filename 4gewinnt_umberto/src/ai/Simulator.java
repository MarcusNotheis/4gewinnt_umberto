package ai;

import java.util.ArrayList;

import models.PlayingField;

/**
 * The Simulator-class needs a Playingfield as substructure to work. Its main
 * task is the administration of all simulations.
 * 
 * @author Stefan Weimann
 */
public class Simulator
{
	private PlayingField playGround;
	private int[][] field;
	private int columnForNextMove;
	private int simulated;
	private int currentColumn;
	private ArrayList<Integer> possibleColumns;
	private SimulatorWorker worker;
	private SimulationInterpreter interpreter;

	/**
	 * Constructs an instance of the Simulator-class
	 * 
	 * @param playingField
	 *            A Playingfield is needed as substructure to work on.
	 */
	public Simulator(PlayingField playingField) {
		playGround = playingField;
		field = playingField.getField();
		possibleColumns = new ArrayList<Integer>();
		interpreter = new SimulationInterpreter();
	}

	/**
	 * @return Returns an integer value, which is between 0 and 6.
	 */
	public int getColumnForNextMove() {
		return columnForNextMove;
	}

	/**
	 * @return Returns all possible columns where you can put in a token.
	 */
	public ArrayList<Integer> getPossibleColumns() {
		return possibleColumns;
	}

	/**
	 * This method starts the real simulation.
	 * 
	 * @param quantity
	 *            You handover a number of simulations for each possible column.
	 */
	public void simulate(int quantity) {
		loadPossibleColumns();
		startSimulation(quantity);
		interpreter.interpretSimulation(quantity);
		columnForNextMove = interpreter.getNextMove();
	}

	private void loadPossibleColumns() {
		int maxRow = playGround.getHEIGHT() - 1;
		for (int i = 0; i < playGround.getWIDTH(); i++) {
			if (field[i][maxRow] == 0) {
				possibleColumns.add(i);
			}
		}
	}

	private void startSimulation(int quantity) {
		worker = new SimulatorWorker(possibleColumns);
		for (currentColumn = 0; currentColumn < possibleColumns.size(); currentColumn++) {
			simulateCurrentPlayGround(quantity);
		}
	}

	private void simulateCurrentPlayGround(int quantity) {
		startThreads(quantity);
	}

	private void startThreads(int quantity) {
		for (simulated = 0; simulated < quantity; simulated++) {
			worker.setField(playGround);
			worker.setSimulationTokenForPlayerOne(possibleColumns
					.get(currentColumn));
			worker.work();
			interpreter.addSimulationResult(worker.getWinner(), possibleColumns
					.get(currentColumn));
		}
	}
}
