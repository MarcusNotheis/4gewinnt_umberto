package ai;

import java.util.ArrayList;

import models.PlayingField;

/**
 * This worker is a construct to simulate exactly one game.<br>
 * Later it will simulate in Threads.
 * 
 * @author Stefan Weimann
 */
public class SimulatorWorker
{
	private PlayingField field;
	private ArrayList<Integer> possibleColumns;
	private ArrayList<Integer> currentPossibleColumns;
	private WinDetector detector;
	private int randomColumn;
	private int actualPlayer;
	private int winner;
	private boolean winnerFound = false;
	private boolean randomNumberFound = false;
	private boolean tokenIsSet;

	/**
	 * Constructs an instance if the SimulatorWorker
	 * 
	 * @param possibleColumns
	 *            The worker needs all possible columns in which he can put a
	 *            token.
	 */
	public SimulatorWorker(ArrayList<Integer> possibleColumns) {
		this.possibleColumns = new ArrayList<Integer>(possibleColumns);
	}

	/**
	 * This method is required to simulate a single game.
	 * 
	 * @param playingField
	 *            The current pLayingfield to simulate on.
	 */
	public void setField(PlayingField playingField) {
		this.field = playingField.clonePlayingField();
	}

	/**
	 * This method has to be executed to set the first token on the current
	 * playingfield.
	 * 
	 * @param column Sets a token on the given column
	 */
	public void setSimulationTokenForPlayerOne(int column) {
		field.setToken(column, 1);
	}

	/**
	 * This method returns the winner of the current simulation
	 * 
	 * @return If nobody has won it will return 0.
	 */
	public int getWinner() {
		return winner;
	}

	/**
	 * This method returns the field after the simulation has finished.
	 * 
	 * @return Returns the playingfield of the finished game.
	 */
	public PlayingField getField() {
		return field;
	}

	/**
	 * This method starts the simulation.<br>
	 * Later this method will be replaced with the run()-method of the
	 * Thread-class
	 */
	public void work() {
		resetClassVariables();
		while (winnerFound == false && currentPossibleColumns.size() > 0) {
			resetRoundVariables();
			findRandomColumn();
			placeToken();
			switchPlayer();
			findWinner();
		}
	}

	private void resetClassVariables() {
		winnerFound = false;
		currentPossibleColumns = new ArrayList<Integer>(possibleColumns);
		detector = new WinDetector();
		actualPlayer = 1;
		winner = 0;
	}

	private void resetRoundVariables() {
		randomNumberFound = false;
		tokenIsSet = false;
	}

	private void findRandomColumn() {
		while (randomNumberFound == false) {
			randomColumn = (int) (Math.random() * 7);
			checkIfRandomIsValidNumber();
		}
	}

	private void checkIfRandomIsValidNumber() {
		if (currentPossibleColumns.contains(randomColumn)) {
			randomNumberFound = true;
		}
	}

	private void placeToken() {
		tokenIsSet = field.setToken(randomColumn, actualPlayer);
		if (!tokenIsSet && currentPossibleColumns.size() > 0) {
			currentPossibleColumns.remove(currentPossibleColumns
					.indexOf(randomColumn));
		}
	}

	private void switchPlayer() {
		if (tokenIsSet) {
			switch (actualPlayer) {
			case 1:
				actualPlayer = 2;
				break;
			case 2:
				actualPlayer = 1;
				break;
			}
		}
	}

	private void findWinner() {
		detector.setPlayGround(field);
		detector.detectWinner();
		winner = detector.getWinner();
		if (winner != 0) {
			winnerFound = true;
		}
	}
}
