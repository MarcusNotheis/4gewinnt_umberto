package home;

import java.util.ArrayList;

import models.Draw;
import models.PlayingField;

/**
 * Manages all about draws.
 * 
 * @author Heußner
 */
public class DrawManagement {

	PlayingField playingField;
	Administration administration;
	Draw draw;
	ArrayList<Draw> drawArrayList;
	ArrayList<Integer> possibleColumns;
	private boolean randomNumberFound;
	private int randomColumn;

	/**
	 * @param administration
	 *            The current Administration class to work with in
	 *            DrawManagement.
	 */
	public DrawManagement(Administration administration) {
		drawArrayList = new ArrayList<Draw>();
		draw = new Draw();
		this.administration = administration;
	}

	/**
	 * manageDraw() manages all abaout a new Draw. It sets a Token on the
	 * PlayingField. It creates a Draw. and it displays the Draw on the UI.
	 * 
	 * @param playingField
	 *            The current PlayingField
	 * @param column
	 *            The column (between 0 and 6) of the current move.
	 * @param player
	 *            1 for Umberto or 2 for an enemy move.
	 */
	public void manageDraw(PlayingField playingField, int column, int player) {
		this.playingField = playingField;
		placeToken(column, player);
		createDraw(player);
		displayDraw();
	}

	private void placeToken(int column, int player) {
		boolean tokenOK;
		tokenOK = playingField.setToken(column, player);
		if (tokenOK == false) {
			System.out
					.println("Draw Management: Fehler beim setzten des Tokens!");
			calcRandomToken();
			if (playingField.checkIfFieldIsFull() == false) {
				playingField.setToken(randomColumn, player);
				return;
			}
			System.out.println("Draw Management: Feld ist voll!");
		}
	}

	private void calcRandomToken() {
		loadPossibleColumns();
		findRandomColumn();
	}

	private void loadPossibleColumns() {
		int[][] field = playingField.getField();
		int maxRow = playingField.getHEIGHT() - 1;
		for (int i = 0; i < playingField.getWIDTH(); i++) {
			if (field[i][maxRow] == 0) {
				possibleColumns.add(i);
			}
		}
	}

	private void findRandomColumn() {
		while (!randomNumberFound && possibleColumns.size() > 0) {
			randomColumn = (int) (Math.random() * 7);
			checkIfRandomIsValidNumber();
		}
	}

	private void checkIfRandomIsValidNumber() {
		if (possibleColumns.contains(randomColumn)) {
			randomNumberFound = true;
		}
	}

	private void createDraw(int player) {
		draw = new Draw();
		draw.setPlayer(player);
		draw.setToken(playingField.getLastToken());
		drawArrayList.add(draw);
		administration.getDatabase().insertDraw(drawArrayList);
		drawArrayList.clear();
	}

	private void displayDraw() {
		administration.getUserInterface().setToken(draw);
	}

	/**
	 * @return Returns the updated PlayingField.
	 */
	public PlayingField getNewPlayingField() {
		return this.playingField;
	}

}
