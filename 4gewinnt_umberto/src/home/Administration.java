package home;

import java.io.IOException;

import models.PlayingField;
import models.Token;
import server.ServerConnector;
import ui.UserInterface;
import ai.GameIntelligence;
import db.Database;

/**
 * Administration is the central Class of Umberto. It includes the central main
 * method to start Umberto and manages all relevant classes.
 * 
 * @author Heußner
 */

public class Administration {

	static Administration administration;
	private ServerConnector serverConnector;
	private UserInterface userInterface;
	private PlayingField playingField;
	private GameIntelligence gameIntelligence;
	private DrawManagement drawManagement;
	private Database database;

	private String path;
	private String playerName = "Umberto";
	private String enemyName;

	private int winner;
	private int player;
	private int nextMove;
	private int enemyMove;
	private int count = 1;

	public boolean umbertoIsStartPlayer;
	public boolean setTokenOK;

	/**
	 * This is the central main method which is used to start Umberto.
	 * 
	 * @param args
	 *            Arguments for executing the main method
	 * @throws IOException
	 *             Raised if the given arguments are invalid
	 */
	public static void main(String[] args) throws IOException {

		administration = new Administration();

	}

	/**
	 * Constructor, which initializes all required objects.
	 */
	public Administration() {
		// Initialisieren aller benötigten Klassen:
		System.out.println("Administration: Start initialize------------");
		userInterface = new UserInterface(this);
		playingField = new PlayingField();
		serverConnector = new ServerConnector();
		gameIntelligence = new GameIntelligence();
		database = new Database();
		drawManagement = new DrawManagement(this);
		System.out.println("Administration: End initialize--------------");
	}

	/**
	 * Sets the configuration parameter for the game.
	 * 
	 * @param serverPath
	 *            Is used to set the directory for the coming game via the
	 *            Server Connector.
	 * @param enemyNameNew
	 *            The enemy name for the coming game.
	 * @param moveTime
	 *            Sets the move time for the coming game via a public Method of
	 *            Game Intelligence.
	 * @param serverPlayerName
	 *            Sets if Umberto is PlayerX or PlayerO on the server for this
	 *            particular game.
	 */
	public void loadConfigurationDataFromGuiForNewGame(String serverPath,
			String enemyNameNew, String serverPlayerName) {
		path = serverPath;
		serverConnector.setPath(path, serverPlayerName);
		enemyName = enemyNameNew;
		database.insertGame(enemyNameNew, "");
		preloadGameIntelligenceObjects();
	}

	private void preloadGameIntelligenceObjects() {

		for (int i = 0; i < 4; i++) {
			playingField.setToken(3, 2);
			gameIntelligence.setField(playingField);
			gameIntelligence.calculateNextMove();
		}
		playingField = new PlayingField();
	}

	/**
	 * @throws IOException
	 *             startGame() is the central Method, which displays the
	 *             programm sequence. It stops when a winner is determined.
	 */
	public void startGame() throws IOException {
		playingField = new PlayingField();
		database.insertSet("");
		while (winner == 0) {
			System.out.println("Administration: ---------new Round---------");
			loadEnemyMove();
			loadUmbertoMove();
		}
		loadWinnerToken();
		setWinnerDB();
	}

	private void loadEnemyMove() throws IOException {
		checkForFile();
		determineNextDraw();
		detectWinner();
	}

	private void loadUmbertoMove() throws IOException {
		if (winner == 0) {
			calculateMove();
			writeServerFile();
			takeDraw(nextMove, player);
			detectWinner();
		}
	}

	/**
	 * Resets the winner to 0 and initializes a new empty PlayingField.
	 */
	public void resetGame() {
		playingField = new PlayingField();
		gameIntelligence = new GameIntelligence();
		drawManagement = new DrawManagement(this);
		winner = 0;
		count = 1;
		System.out.println("Administration: Reset Game!");
	}

	/**
	 * @return Returns the current Database to work with.
	 */
	public Database getDatabase() {
		return database;
	}

	/**
	 * @return Returns the current UserInterface to work with.
	 */
	public UserInterface getUserInterface() {
		return userInterface;
	}

	private void checkForFile() throws IOException {
		enemyMove = serverConnector.checkForUpdate();
		player = 2;
	}

	private boolean checkIfUmbertoIsStartPlayer() {
		if (enemyMove == -1) {
			return true;
		} else {
			return false;
		}
	}

	private void determineNextDraw() {
		String startingPlayer = playerName;
		if (!checkIfUmbertoIsStartPlayer()) {
			player = 2;
			takeDraw(enemyMove, player);
			startingPlayer = enemyName;
		}
		if (count == 1) {
			count++;
			database.updateGame(startingPlayer);
		}
	}

	private void takeDraw(int column, int player) {
		drawManagement.manageDraw(playingField, column, player);
	}

	private void calculateMove() {
		gameIntelligence.setField(playingField);
		nextMove = gameIntelligence.calculateNextMove();
		player = 1;
	}

	private void writeServerFile() throws IOException {
		serverConnector.saveMove(nextMove);
	}

	private void detectWinner() throws IOException {
		winner = gameIntelligence.getWinner(playingField);
		System.out.println("Administration: Winner: " + winner + "!");
	}

	private void loadWinnerToken() {
		userInterface.setWinningToken(gameIntelligence.getWinDetector()
				.getWinnertoken());
		database.insertWinningToken("" + userInterface.getSetCount(),
				loadWinnerTokenForDB());
	}

	private String[] loadWinnerTokenForDB() {
		String[] tokens = new String[4];
		Token[] winnerTokens = gameIntelligence.getWinDetector()
				.getWinnertoken();
		for (int i = 0; i < 4; i++) {
			tokens[i] = "" + winnerTokens[i].getyCoordinate() + ","
					+ winnerTokens[i].getxCoordinate();
		}
		return tokens;
	}

	private void setWinnerDB() {
		if (winner == 1) {
			database.updateSet("Umberto");
		} else {
			database.updateSet(enemyName);
		}
	}
}
