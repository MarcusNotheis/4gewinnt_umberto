package models;

public class Draw {
	
	private Token token;
	private int player;
	
	public Token getToken(){
		return token;
	}
	
	public void setToken(Token token){
		this.token = token;
	}
	
	public int getPlayer(){
		return player;
	}
	
	public void setPlayer(int player) {
		this.player = player;
	}

}
