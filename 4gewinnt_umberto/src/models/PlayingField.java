package models;

/**
 * @author Stefan Weimann
 */
public class PlayingField
{
	private final int WIDTH = 7;
	private final int HEIGHT = 6;
	private int field[][];
	private Token lastToken;
	private PlayingField playingFieldClone;

	public PlayingField() {
		field = new int[WIDTH][HEIGHT];
		lastToken = new Token();
	}

	public PlayingField clonePlayingField() {
		playingFieldClone = new PlayingField();
		cloneField(this.getField());
		return playingFieldClone;
	}

	private void cloneField(int[][] temp) {
		for (int i = 0; i < playingFieldClone.getHEIGHT(); i++) {
			for (int o = 0; o < playingFieldClone.getWIDTH(); o++) {
				if (temp[o][i] != 0) {
					playingFieldClone.setToken(o, temp[o][i]);
				}
			}
		}
	}

	public int[][] getField() {
		return field;
	}

	public int getWIDTH() {
		return WIDTH;
	}

	public int getHEIGHT() {
		return HEIGHT;
	}

	public Token getLastToken() {
		return lastToken;
	}

	/**
	 * @param column
	 *            In this column a token will be placed
	 * @param player
	 *            Name a player with <b>1</b> or <b>2</b>, who put this token
	 * @throws ArrayIndexOutOfBoundsException
	 *             Exception will be thrown if you try to insert a Token in a
	 *             non existing column
	 * @return If <b>true</b> the token is successfully placed in the field. If
	 *         <b>false</b> the column is already full.
	 */
	public boolean setToken(int column, int player)
			throws ArrayIndexOutOfBoundsException {
		for (int i = 0; i < HEIGHT; i++) {
			if (field[column][i] == 0) {
				field[column][i] = player;
				lastToken.setxCoordinate(column);
				lastToken.setyCoordinate(i);
				return true;
			}
		}
		return false;
	}

	public boolean checkIfFieldIsFull() {
		boolean isFull = true;
		for (int i = 0; i < WIDTH; i++) {
			if (field[i][HEIGHT - 1] == 0) {
				isFull = false;
			}
		}

		return isFull;
	}
}
