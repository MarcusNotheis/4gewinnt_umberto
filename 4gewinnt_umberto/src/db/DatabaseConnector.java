package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * This class creates a Connection to the database. It provides read and write
 * access to the database.
 * 
 * @author MarcusNotheis
 *
 */
public class DatabaseConnector implements java.io.Serializable {

	private static final long serialVersionUID = -7794800098420880331L;

	private Connection connection = null;
	private String dbPath = "umbertoDB";
	private String user = "reihe2";
	private String password = "umberto";

	/**
	 * Creates Connection to DB
	 */
	public void createConnectionToDB() {

		// 1. Load driver for HSQLDB
		try {
			Class.forName("org.hsqldb.jdbcDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Database Connector: Unable to load driver for HSQLDB!");
			return;
		}

		// 2. set up connection to DB
		try {
			connection = DriverManager.getConnection("jdbc:hsqldb:file:"
					+ dbPath + ";shutdown=true", user, password);
			System.out
					.println("Database Connector: Created Connection to Game DB...");

		} catch (SQLException e) {
			System.err
					.println("Database Connector: Unable to connect to database");
			System.err.println("Database Connector: SQLException: "
					+ e.getMessage());
			System.err.println("Database Connector: SQLState: "
					+ e.getSQLState());
			System.err.println("Database Connector: VendorError: "
					+ e.getErrorCode());
		}

		// check if all tables exist
		DatabaseQueries dbQueries = new DatabaseQueries();
		createTable(dbQueries.createTableGames());
		createTable(dbQueries.createTableSets());
		createTable(dbQueries.createTableDraws());
		createTable(dbQueries.createTableWinningTokens());
		System.out
				.println("Database Connector: All database tables checked...");
	}

	/**
	 * Gets all data from DB with query
	 * 
	 * @param query
	 *            The SQL query
	 * @return ArrayList with all results of select Statement
	 */
	public ArrayList<String[]> getDatasetFromDatabase(String query) {

		if (connection == null) {
			createConnectionToDB();
		}
		System.out.println(query);

		// ArrayList of String Arrays as storage for SELECT result
		ArrayList<String[]> resultList = new ArrayList<String[]>();

		// create Statement
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(query);

			int columns = rs.getMetaData().getColumnCount();

			// Append each Table Row to resultList
			while (rs.next()) {
				// create new ArrayList for dynamic reaction to ResultSet-Size
				ArrayList<String> temp = new ArrayList<String>();
				// add each column to temp-ArrayList
				for (int i = 1; i <= columns; i++) {
					temp.add(rs.getString(i));
				}
				temp.trimToSize();
				// Convert temp to String[] Array
				String[] array = new String[temp.size()];
				array = (String[]) temp.toArray(array);
				// Add array to ResultList
				resultList.add(array);
			}
			// optimize size of resultList
			resultList.trimToSize();

			// Close result and statement
			rs.close();
			statement.close();
		} catch (SQLException e) {
			System.err
					.println("Database Connector: Failed to execute query on database");
			System.err.println("Database Connector: SQLException: "
					+ e.getMessage());
			System.err.println("Database Connector: SQLState: "
					+ e.getSQLState());
		}
		return resultList;
	}

	/**
	 * Creates or updates an entry in DB
	 * 
	 * @param query
	 *            The SQL query
	 */
	public void writeToDatabase(String query) {

		if (connection == null) {
			createConnectionToDB();
		}
		System.out.println("Database Connector: " + query);

		try {
			Statement statement = connection.createStatement();
			int rows = statement.executeUpdate(query);

			if (rows == 0) {
				System.out
						.println("Database Connector: No changes in database");
			} else {
				System.out.println("Database Connector: " + rows
						+ " entries changed in database");
			}
			statement.close();
		} catch (SQLException e) {
			System.err
					.println("Database Connector: Writing to database failed!");
			System.err.println("Database Connector: SQLException: "
					+ e.getMessage());
			System.err.println("Database Connector: SQLState: "
					+ e.getSQLState());
		}
	}

	/**
	 * Creates Tables in DB
	 * 
	 * @param query
	 *            The SQL query
	 */
	public void createTable(String query) {

		if (connection == null) {
			createConnectionToDB();
		}

		System.out.println("Database Connector: " + query);

		try {
			Statement statement = connection.createStatement();
			statement.execute(query);
			System.out.println("Database Connector: Database table created");
		} catch (SQLException e) {
			System.err
					.println("Database Connector: Creation of database table failed!");
			System.err.println("Database Connector: SQLException: "
					+ e.getMessage());
			System.err.println("Database Connector: SQLState: "
					+ e.getSQLState());
		}
	}

	/**
	 * Closes the active DB connection
	 */
	public void closeDatabaseConnection() {

		if (connection != null) {
			try {
				connection.close();
				System.out
						.println("Database Connector: Connection successfully closed...");
			} catch (SQLException e) {
				System.err
						.println("Database Connector: Failed to close database connection");
			}
		}

	}

}
