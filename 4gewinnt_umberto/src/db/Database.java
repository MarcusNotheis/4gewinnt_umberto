package db;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import models.Draw;
import models.Token;

/**
 * The Database class provides futher actions for the database. These methods
 * are called if queries need further information.
 * 
 * @author Marcus Notheis
 */
public class Database {

	private DatabaseConnector db;
	private DatabaseQueries dbQueries;
	private Properties properties;

	public Database() {
		db = new DatabaseConnector();
		db.createConnectionToDB();
		dbQueries = new DatabaseQueries();
		properties = dbQueries.getProperties();
	}

	/*
	 * ############## S E L E C T S T A T E M E N T S ##############
	 */

	/**
	 * Selects all games
	 * 
	 * @return ArrayList of games with structure Id, Opponent, Winner,
	 *         Startplayer
	 */
	public ArrayList<String[]> selectAllGames() {
		return db.getDatasetFromDatabase(dbQueries.getAllGames());
	}

	/**
	 * Returns a single game
	 * 
	 * @param game_id
	 *            The id of the game
	 * @return Returns a single game with Id, Opponent, Winner, Startplayer
	 */
	public String[] selectGameById(String game_id) {
		ArrayList<String[]> dbResult = db.getDatasetFromDatabase(dbQueries
				.getGameById(game_id));
		String[] array = dbResult.get(0);
		return array;

	}

	/**
	 * Returns the opponent for a specific game
	 * 
	 * @param game_id
	 *            The Game-Id
	 * @return Name of the opponent
	 */
	public String selectOpponentNameForGame(String game_id) {
		ArrayList<String[]> result = db.getDatasetFromDatabase(dbQueries
				.getGameById(game_id));
		String opponent = result.get(0)[1];
		return opponent;
	}

	/**
	 * Selects all Draws for a specific game
	 * 
	 * @param gameId
	 *            Id of the game
	 * @return ArrayList of all draws for a specific game
	 */
	public ArrayList<String[]> selectAllDrawsForGame(String gameId) {
		return db.getDatasetFromDatabase(dbQueries.getAllDrawsForGame(gameId));
	}

	/**
	 * Select All Draws for a set in a game
	 * 
	 * @param gameId
	 *            The game ID
	 * @param setId
	 *            The set ID
	 * @return List of all draws for a specfic set in a game (x,y, player)
	 */
	public ArrayList<String[]> selectAllDrawsForGameInSet(String gameId,
			String setId) {

		ArrayList<String[]> sets = db.getDatasetFromDatabase(dbQueries
				.getSetsForGame(gameId));
		sets.trimToSize();

		ArrayList<String[]> result = new ArrayList<String[]>();

		try {
			if (setId.equals("1")) {
				String set = sets.get(0)[0];
				result = db.getDatasetFromDatabase(dbQueries
						.getAllDrawsForSet(set));
			}
			if (setId.equals("2")) {
				String set = sets.get(1)[0];
				result = db.getDatasetFromDatabase(dbQueries
						.getAllDrawsForSet(set));
			}
			if (setId.equals("3")) {
				String set = sets.get(2)[0];
				result = db.getDatasetFromDatabase(dbQueries
						.getAllDrawsForSet(set));
			}
		} catch (IndexOutOfBoundsException e) {
			System.err.println("Index not available");
		}

		return result;
	}

	/*
	 * ############## I N S E R T S T A T E M E N T S ##############
	 */

	/**
	 * Inserts a new game
	 * 
	 * @param opponent
	 *            The opponents name
	 * @param start
	 *            The name of the starting player
	 */
	public void insertGame(String opponent, String start) {
		db.writeToDatabase(dbQueries.insertGame(opponent, start));
	}

	/**
	 * Inserts a new set
	 * 
	 * @param winner
	 *            Name of the set winner
	 */
	public void insertSet(String winner) {
		int game_id = getLastIndexOfTable(properties
				.getProperty("game_table_name"));
		db.writeToDatabase(dbQueries.insertSet(game_id, winner));
	}

	/**
	 * Inserts a new draw
	 * 
	 * @param drawArrayList
	 *            ArrayList of Draws
	 */
	public void insertDraw(ArrayList<Draw> drawArrayList) {
		int set_id = getLastIndexOfTable(properties
				.getProperty("set_table_name"));
		db.writeToDatabase(dbQueries.insertDraw(set_id, drawArrayList));
	}

	/**
	 * Inserts a new winning token
	 * 
	 * @param game
	 *            GameId
	 * @param set
	 *            Set (numbered 1, 2, 3)
	 * @param tokens
	 *            StringArray of tokens. Format: {"0,1", "0,2", "0,3", "0,4"}
	 */
	public void insertWinningToken(String set, String[] tokens) {
		int gameInt = getLastIndexOfTable(properties.getProperty("game_table_name"));
		String game = gameInt + "";
		db.writeToDatabase(dbQueries.insertWinningToken(game, set, tokens));
	}

	/*
	 * ############## U P D A T E S T A T E M E N T S ##############
	 */

	/**
	 * Updates game table with start player
	 * 
	 * @param start
	 *            Name of the player who started the game
	 */
	public void updateGame(String start) {
		int game_id = getLastIndexOfTable(properties
				.getProperty("game_table_name"));
		db.writeToDatabase(dbQueries.updateGame(game_id, start));
	}

	/**
	 * Updates game table with winner
	 * 
	 * @param game_id
	 *            The game_id where the update should take place
	 * @param winner
	 *            The winners name
	 */
	public void updateWinner(String game_id, String winner) {
		db.writeToDatabase(dbQueries.updateGameWinner(game_id, winner));
	}

	/**
	 * Updates the set table with the winner of the latest set
	 * 
	 * @param winner
	 *            Name of the player who won the set
	 */
	public void updateSet(String winner) {
		int set_id = getLastIndexOfTable(properties
				.getProperty("set_table_name"));
		db.writeToDatabase(dbQueries.updateSet(set_id, winner));
	}

	/*
	 * ############## C R E A T E S T A T E M E N T S ##############
	 */

	/**
	 * Creates table umberto_games
	 */
	public void createTableGame() {
		db.createTable(dbQueries.createTableGames());
	}

	/**
	 * Creates table umberto_sets
	 */
	public void createTableSet() {
		db.createTable(dbQueries.createTableSets());
	}

	/**
	 * Creates table umberto_draws
	 */
	public void createTableDraws() {
		db.createTable(dbQueries.createTableDraws());
	}

	/**
	 * Creates table umberto_winning_tokens
	 */
	public void createTableWinningToken() {
		db.createTable(dbQueries.createTableWinningTokens());
	}

	/**
	 * Gets the last index of a given table
	 * 
	 * @param table
	 *            The table name
	 * @return The last index of the given table
	 */
	public int getLastIndexOfTable(String table) {

		DatabaseQueries dbQueries = new DatabaseQueries();

		ArrayList<String[]> result = db.getDatasetFromDatabase(dbQueries
				.getLastIndexOfTable(table));

		String temp = result.get(0)[0];

		int index = new Integer(temp);

		return index;
	}

	/**
	 * Creates data for game overview. Collects Opponent, Date and Score
	 * 
	 * @return List of all games with opponent, date and score. ArrayList
	 *         contains Strings of {Opponent, Date, Score}
	 */
	public ArrayList<String[]> getGameDataForStartScreen() {

		DatabaseQueries dbQueries = new DatabaseQueries();

		ArrayList<String[]> resultList = new ArrayList<String[]>();

		// 1. Get all Game Data (ID | Opponent | Winner)
		ArrayList<String[]> games = db.getDatasetFromDatabase(dbQueries
				.getAllGames());
		for (String[] game : games) {
			String[] resultEntry = new String[4];
			resultEntry[0] = game[0];
			resultEntry[1] = game[1];
			resultList.add(resultEntry);
		}

		// 2. Get Date for Game (timestamp)
		for (String[] result : resultList) {

			ArrayList<String[]> dates = db.getDatasetFromDatabase(dbQueries
					.getTimestampOfGame(result[0]));
			try {
				// create timestamp out of String
				Timestamp time = Timestamp.valueOf(dates.get(0)[0]);

				// create calendar and get dates
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(time.getTime());

				int month = calendar.get(Calendar.MONTH);
				month++;

				String date = calendar.get(Calendar.DAY_OF_MONTH) + "." + month
						+ "." + calendar.get(Calendar.YEAR);

				result[2] = date;
			} catch (IndexOutOfBoundsException e) {
				System.err.println("No date available. Set date to 01.01.1970");
				result[2] = "01.01.1970";
			}
		}

		// 3. Calculate score for Games
		for (String[] result : resultList) {
			ArrayList<String[]> scores = db.getDatasetFromDatabase(dbQueries
					.getWinnerOfSet(result[0]));

			int umbertoScore = 0;
			int opponentScore = 0;

			for (String[] score : scores) {
				if (score[0].equals(result[0])) {
					if (score[1].equals("")) {
						continue;
					}
					if (score[1].equalsIgnoreCase("Umberto")) {
						umbertoScore++;
						continue;
					}
					opponentScore++;
				}
			}
			String finalScore = umbertoScore + " : " + opponentScore;
			result[3] = finalScore;
		}

		return resultList;
	}

	/**
	 * Returns the winning tokens for a specific game
	 * 
	 * @param gameId
	 *            The Game ID
	 * @param setId
	 *            The Set ID
	 * @return Array of all 4 winning tokens
	 */
	public Token[] getWinningTokensForGame(String gameId, String setId) {

		Token[] result = new Token[4];

		ArrayList<String[]> dbResult = db.getDatasetFromDatabase(dbQueries
				.getWinningTokensForGame(gameId, setId));

		for (String[] entry : dbResult) {
			String temp = entry[1];
			String array[] = temp.split("/");

			// Coordiantes are stored as (y-coordiante, x-coordiante)
			String[] coord1 = array[0].split(",");
			String[] coord2 = array[1].split(",");
			String[] coord3 = array[2].split(",");
			String[] coord4 = array[3].split(",");

			Token tok1 = new Token();
			tok1.setyCoordinate(new Integer(coord1[0]));
			tok1.setxCoordinate(new Integer(coord1[1]));
			result[0] = tok1;

			Token tok2 = new Token();
			tok2.setyCoordinate(new Integer(coord2[0]));
			tok2.setxCoordinate(new Integer(coord2[1]));
			result[1] = tok2;

			Token tok3 = new Token();
			tok3.setyCoordinate(new Integer(coord3[0]));
			tok3.setxCoordinate(new Integer(coord3[1]));
			result[2] = tok3;

			Token tok4 = new Token();
			tok4.setyCoordinate(new Integer(coord4[0]));
			tok4.setxCoordinate(new Integer(coord4[1]));
			result[3] = tok4;
		}

		return result;

	}

	/**
	 * Closes database connection
	 */
	public void closeConnection() {
		db.closeDatabaseConnection();
	}

}
