package db;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import models.Draw;

/**
 * This class provides all the queries to executed on the database.
 * 
 * @author MarcusNotheis
 *
 */
public class DatabaseQueries {

	private Properties properties = null;

	/**
	 * Constructor for DB Queries. Loads DatabaseContract.properties file
	 */
	public DatabaseQueries() {

		properties = new Properties();
		try {
			properties.load(DatabaseQueries.class
					.getResourceAsStream("DatabaseContract.properties"));
		} catch (IOException e) {
			System.err.println("Failed to load DatabaseContract!");
		}
	}
	// }

	/*
	 * ########################################################################
	 * INSERT Data
	 */

	/**
	 * Inserts a new game
	 * 
	 * @param opponent
	 *            The playername of the opponent
	 * @param start_player
	 *            The name of the starting player
	 * @return Query for inserting a new game
	 */
	public String insertGame(String opponent, String start_player) {

		String query = "";
		query = "INSERT INTO " + properties.getProperty("game_table_name")
				+ " (" + properties.getProperty("game_column_opponent") + ", "
				+ properties.getProperty("game_column_winner") + ", "
				+ properties.getProperty("game_column_start_player") + ") "
				+ " VALUES ('" + opponent + "', '-1', '" + start_player + "');";

		return query;
	}

	/**
	 * Insets a new set
	 * 
	 * @param game_id
	 *            The index of the table where the update should take place
	 * @param winner
	 *            Winner of the set
	 * @return Query for inserting a new set
	 */
	public String insertSet(int game_id, String winner) {

		String query = "";
		query = "INSERT INTO " + properties.getProperty("set_table_name")
				+ " (" + properties.getProperty("set_column_game") + ", "
				+ properties.getProperty("set_column_winner") + ") "
				+ " VALUES ('" + game_id + "', '" + winner + "');";

		return query;
	}

	/**
	 * Insers a new draw
	 * 
	 * @param set_id
	 *            The index of the table where the update should take place
	 * @param drawArrayList
	 *            Arraylist of all draws to be inserted
	 * @return Query for inserting new draws
	 */
	public String insertDraw(int set_id, ArrayList<Draw> drawArrayList) {

		// ArrayList<Draw> contains playername and token

		String query = "INSERT INTO "
				+ properties.getProperty("draw_table_name") + " ("
				+ properties.getProperty("draw_column_set") + ", "
				+ properties.getProperty("draw_column_time") + ", "
				+ properties.getProperty("draw_column_x") + ", "
				+ properties.getProperty("draw_column_y") + ", "
				+ properties.getProperty("draw_column_player") + ") VALUES ";

		for (Draw draw : drawArrayList) {
			// create new Timestamp
			Date date = new Date();
			Timestamp time = new Timestamp(date.getTime());
			// Extend Query
			query += "('" + set_id + "', '" + time + "', '"
					+ draw.getToken().getxCoordinate() + "', '"
					+ draw.getToken().getyCoordinate() + "', '"
					+ draw.getPlayer() + "'), ";

			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				System.err.println("Error while trying to pause thread!");
			}
		}

		query = query.substring(0, query.length() - 2);
		query += ";";

		return query;
	}

	/**
	 * Insers a new dataset into table winning tokens
	 * 
	 * @param set
	 *            The Set Id
	 * @param tokens
	 *            StringArray of tokens. Format: y,x/y,x/y,x/y,x
	 * @return Query for inserting winning tokens
	 */
	public String insertWinningToken(String game, String set, String[] tokens) {
		String query = "";

		String tok = tokens[0] + "/" + tokens[1] + "/" + tokens[2] + "/"
				+ tokens[3];

		query = "INSERT INTO "
				+ properties.getProperty("winningToken_table_name") + "("
				+ properties.getProperty("winningToken_column_game") + ", "
				+ properties.getProperty("winningToken_column_set") + ", "
				+ properties.getProperty("winningToken_column_tokens")
				+ ") VALUES('" + game + "', '" + set + "', '" + tok + "');";

		return query;
	}

	/*
	 * ########################################################################
	 * SELECT Statements
	 */

	/**
	 * Query for getting all games
	 * 
	 * @return Query for selecting all games. Will return (ID, Opponent, Winner)
	 */
	public String getAllGames() {

		String query = "";
		query = "SELECT * FROM " + properties.getProperty("game_table_name");
		System.out.println(query);

		return query;
	}

	/**
	 * Select single game with Id
	 * 
	 * @param game_id
	 *            The id of the game
	 * @return Query for getting a single game
	 */
	public String getGameById(String game_id) {
		String query = "";
		query = "SELECT * FROM " + properties.getProperty("game_table_name")
				+ " WHERE " + properties.getProperty("game_column_id") + "="
				+ game_id + ";";
		return query;
	}

	/**
	 * Query for getting all draws for a given game
	 * 
	 * @param id
	 *            ID for Game
	 * @return Query for selecting all draws for specific game. Will return
	 *         (Player, Column, Row)
	 */
	public String getAllDrawsForGame(String id) {

		String query = "";
		query = "SELECT " + properties.getProperty("draw_column_player") + ", "
				+ properties.getProperty("draw_column_x") + ", "
				+ properties.getProperty("draw_column_y") + " FROM "
				+ properties.getProperty("game_table_name") + " INNER JOIN "
				+ properties.getProperty("set_table_name") + " ON "
				+ properties.getProperty("game_table_name") + "."
				+ properties.getProperty("game_column_id") + "="
				+ properties.getProperty("set_table_name") + "."
				+ properties.getProperty("set_column_game") + " INNER JOIN "
				+ properties.getProperty("draw_table_name") + " ON "
				+ properties.getProperty("set_table_name") + "."
				+ properties.getProperty("set_column_id") + "="
				+ properties.getProperty("draw_table_name") + "."
				+ properties.getProperty("draw_column_set") + " WHERE "
				+ properties.getProperty("game_table_name") + "."
				+ properties.getProperty("game_column_id") + "=" + id;

		return query;
	}

	/**
	 * Query returns the last index of a table
	 * 
	 * @param table
	 *            The table
	 * @return Query for getting the last index of a given table
	 */
	public String getLastIndexOfTable(String table) {

		String query = "";
		query = "SELECT MAX(id) FROM " + table;
		return query;
	}

	/**
	 * Query for returning the timestamp of a given game
	 * 
	 * @param id
	 *            The game id
	 * @return query for returning timestamps (timestamp)
	 */
	public String getTimestampOfGame(String id) {

		String query = "";
		query = "SELECT " + properties.getProperty("draw_column_time")
				+ " FROM " + properties.getProperty("game_table_name")
				+ " INNER JOIN " + properties.getProperty("set_table_name")
				+ " ON " + properties.getProperty("game_table_name") + "."
				+ properties.getProperty("game_column_id") + "="
				+ properties.getProperty("set_table_name") + "."
				+ properties.getProperty("set_column_game") + " INNER JOIN "
				+ properties.getProperty("draw_table_name") + " ON "
				+ properties.getProperty("set_table_name") + "."
				+ properties.getProperty("set_column_id") + "="
				+ properties.getProperty("draw_table_name") + "."
				+ properties.getProperty("draw_column_set") + " WHERE "
				+ properties.getProperty("game_table_name") + "."
				+ properties.getProperty("game_column_id") + "=" + id
				+ " ORDER BY " + properties.getProperty("draw_column_time")
				+ " DESC " + "LIMIT 1;";
		return query;
	}

	/**
	 * Gets all Sets for a specific game
	 * 
	 * @param id
	 *            The Game Id
	 * @return Query for getting all sets for a specific game
	 */
	public String getWinnerOfSet(String id) {

		String query = "";

		query = "SELECT " + properties.getProperty("game_column_id") + ", "
				+ properties.getProperty("set_column_winner") + " FROM "
				+ properties.getProperty("game_table_name") + " INNER JOIN "
				+ properties.getProperty("set_table_name") + " ON "
				+ properties.getProperty("game_table_name") + "."
				+ properties.getProperty("game_column_id") + "="
				+ properties.getProperty("set_table_name") + "."
				+ properties.getProperty("set_column_game") + " WHERE "
				+ properties.getProperty("set_table_name") + "."
				+ properties.getProperty("set_column_game") + "=" + id + ";";

		return query;
	}

	/**
	 * Select winning tokens for game
	 * 
	 * @param gameId
	 *            The Game Id
	 * @param setId
	 *            The Set Id (1,2 or 3)
	 * @return Query for getting the winning tokes for a specific game
	 */
	public String getWinningTokensForGame(String gameId, String setId) {

		String query = "";

		query = "SELECT " + properties.getProperty("winningToken_column_set")
				+ ", " + properties.getProperty("winningToken_column_tokens")
				+ " FROM " + properties.getProperty("winningToken_table_name")
				+ " WHERE "
				+ properties.getProperty("winningToken_column_game") + "="
				+ gameId + " AND "
				+ properties.getProperty("winningToken_column_set") + "="
				+ setId + ";";

		return query;
	}

	/**
	 * Select Draws for specific Set
	 * 
	 * @param setId
	 * @return
	 */
	public String getAllDrawsForSet(String setId) {
		String query = "";
		query = "SELECT " + properties.getProperty("draw_column_x") + ", "
				+ properties.getProperty("draw_column_y") + ", "
				+ properties.getProperty("draw_column_player") + " FROM "
				+ properties.getProperty("draw_table_name") + " WHERE "
				+ properties.getProperty("draw_column_set") + "=" + setId + ";";
		return query;
	}

	/**
	 * Select all sets for a specific game
	 * 
	 * @param gameId
	 *            The game ID
	 * @return Query for selecting all sets for game
	 */
	public String getSetsForGame(String gameId) {
		String query = "";
		query = "SELECT " + properties.getProperty("set_column_id") + " FROM "
				+ properties.getProperty("set_table_name") + " WHERE "
				+ properties.getProperty("set_column_game") + "=" + gameId
				+ ";";
		return query;
	}

	/*
	 * ########################################################################
	 * UPDATE Statements
	 */

	/**
	 * Updates a game with a start player
	 * 
	 * @param index
	 *            The index of the table where the update should take place
	 * @param startplayer
	 *            Name of the player who started the game
	 * @return Query for updating a game
	 */
	public String updateGame(int index, String startplayer) {
		String query = "";

		query = "UPDATE " + properties.getProperty("game_table_name") + " SET "
				+ properties.getProperty("game_column_start_player") + "='"
				+ startplayer + "' WHERE "
				+ properties.getProperty("game_column_id") + "='" + index
				+ "';";

		return query;
	}

	/**
	 * Updates winner of a game
	 * 
	 * @param index
	 *            The index of the table where the update should take place
	 * @param winner
	 *            The new winner of a specific game
	 * @return Query for updating the winner of a game
	 */
	public String updateGameWinner(String index, String winner) {
		String query = "";
		query = "UPDATE " + properties.getProperty("game_table_name") + " SET "
				+ properties.getProperty("game_column_winner") + "='" + winner
				+ "' WHERE " + properties.getProperty("game_column_id") + "='"
				+ index + "';";
		return query;
	}

	/**
	 * Updates a set with a winner
	 * 
	 * @param index
	 *            The index of the table where the update should take place
	 * @param winner
	 *            Name of the player who won the set
	 * @return Query for updating a set
	 */
	public String updateSet(int index, String winner) {
		String query = "";

		query = "UPDATE " + properties.getProperty("set_table_name") + " SET "
				+ properties.getProperty("set_column_winner") + "='" + winner
				+ "' " + "WHERE " + properties.getProperty("set_column_id")
				+ "='" + index + "';";

		return query;
	}

	/*
	 * ########################################################################
	 * CREATE TABLE Statements
	 */

	/**
	 * Query for creating the table for games
	 * 
	 * @return Query for creating the Game-Table
	 */
	public String createTableGames() {

		String query = "CREATE TABLE IF NOT EXISTS "
				+ properties.getProperty("game_table_name") + " ("
				+ properties.getProperty("game_column_id")
				+ " int NOT NULL IDENTITY, "
				+ properties.getProperty("game_column_opponent")
				+ " VARCHAR(25) NOT NULL, "
				+ properties.getProperty("game_column_start_player")
				+ " VARCHAR(25) NOT NULL, "
				+ properties.getProperty("game_column_winner")
				+ " VARCHAR(25) NOT NULL)";

		return query;
	}

	/**
	 * Query for creating the table for sets
	 * 
	 * @return Query for creating the Set-Table
	 */
	public String createTableSets() {

		String query = "CREATE TABLE IF NOT EXISTS "
				+ properties.getProperty("set_table_name") + " ("
				+ properties.getProperty("set_column_id")
				+ " int NOT NULL IDENTITY, "
				+ properties.getProperty("set_column_game") + " int NOT NULL "
				+ "FOREIGN KEY REFERENCES "
				+ properties.getProperty("game_table_name") + "("
				+ properties.getProperty("game_column_id") + "), "
				+ properties.getProperty("set_column_winner")
				+ " VARCHAR(25));";

		return query;
	}

	/**
	 * Query for creating the table for draws
	 * 
	 * @return Query for creating the Draw-Table
	 */
	public String createTableDraws() {

		String query = "CREATE TABLE IF NOT EXISTS "
				+ properties.getProperty("draw_table_name") + " ("
				+ properties.getProperty("draw_column_id")
				+ " int NOT NULL IDENTITY, "
				+ properties.getProperty("draw_column_set") + " int NOT NULL "
				+ "FOREIGN KEY REFERENCES "
				+ properties.getProperty("set_table_name") + "("
				+ properties.getProperty("set_column_id") + "), "
				+ properties.getProperty("draw_column_time") + " TIMESTAMP, "
				+ properties.getProperty("draw_column_x") + " int, "
				+ properties.getProperty("draw_column_y") + " int, "
				+ properties.getProperty("draw_column_player")
				+ " VARCHAR(25));";

		return query;
	}

	/**
	 * Creates table for winning tokens
	 * 
	 * @return Query for creating the winning tokens table
	 */
	public String createTableWinningTokens() {
		String query = "";
		query = "CREATE TABLE IF NOT EXISTS "
				+ properties.getProperty("winningToken_table_name") + " ("
				+ properties.getProperty("winningToken_column_id")
				+ " int NOT NULL IDENTITY, "
				+ properties.getProperty("winningToken_column_game")
				+ " int NOT NULL " + "FOREIGN KEY REFERENCES "
				+ properties.getProperty("game_table_name") + "("
				+ properties.getProperty("game_column_id") + "), "
				+ properties.getProperty("winningToken_column_set")
				+ " int NOT NULL, "
				+ properties.getProperty("winningToken_column_tokens")
				+ " VARCHAR(25));";

		return query;
	}

	public Properties getProperties() {
		return properties;
	}

}
