package tests;

import junit.framework.TestCase;
import models.PlayingField;

import org.junit.Before;
import org.junit.Test;

public class PlayingFieldTest extends TestCase{

	private PlayingField field;
	
	@Before
	public void setUp() throws Exception
	{
		field = new PlayingField();	
	}

	@Test
	public void testSetTokenOnFullColumn()
	{
		FillColumnFour();
		TryToPutOneMoreToken();
	}
	
	private void FillColumnFour()
	{
		assertTrue(field.setToken(4, 1));
		assertTrue(field.setToken(4, 1));
		assertTrue(field.setToken(4, 1));
		assertTrue(field.setToken(4, 1));
		assertTrue(field.setToken(4, 1));
		assertTrue(field.setToken(4, 1));
	}
	
	private void TryToPutOneMoreToken()
	{
		assertFalse(field.setToken(4, 1));		
	}
}
