package tests;

import java.util.ArrayList;

import junit.framework.TestCase;
import models.PlayingField;

import org.junit.Before;
import org.junit.Test;

import ai.Simulator;

public class SimulatorTest extends TestCase
{
	PlayingField playingField;
	Simulator simulator;
	ArrayList<Integer> expectedList;
	
	@Before
	public void setUp() throws Exception
	{
		playingField = new PlayingField();
		setUpPlayingField();
		loadExpectedList();
		simulator = new Simulator(playingField);
	}
	
	private void setUpPlayingField()
	{
		playingField.setToken(0, 1);
		playingField.setToken(0, 2);
		playingField.setToken(0, 1);
		playingField.setToken(0, 2);
		playingField.setToken(0, 1);
		playingField.setToken(0, 2);
	}
	
	private void loadExpectedList()
	{
		expectedList = new ArrayList<Integer>();
		expectedList.add(1);
		expectedList.add(2);
		expectedList.add(3);
		expectedList.add(4);
		expectedList.add(5);
		expectedList.add(6);
	}

	@Test
	public void testPossibleColumns() {
		simulator.simulate(2000);
		assertEquals(expectedList, simulator.getPossibleColumns());
	}

}
