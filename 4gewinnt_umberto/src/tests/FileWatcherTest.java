package tests;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import server.FileWatcher;

public class FileWatcherTest extends TestCase {
	
	FileWatcher watcher = null;
	Boolean run = null;
	
	@Before
	public void setUp() throws Exception{
		watcher = new FileWatcher("test.xml");
		run = true;
	}

	@Test
	public void runFileWatcher(){
		while(run){
			watcher.run();
		}
		
	}

}
