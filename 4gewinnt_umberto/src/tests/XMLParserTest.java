package tests;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import server.XMLParser;

public class XMLParserTest extends TestCase{

	private XMLParser parser;
	
	@Before
	public void setUp() throws Exception{
		parser = new XMLParser();
	}

	@Test
	public void testConnectToDB(){
	
		assertEquals(5, parser.parseEnemyMoveOfXML("src/tests/ServerFileTest.xml"));
		
	}

}
