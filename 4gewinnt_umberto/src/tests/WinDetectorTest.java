package tests;

import junit.framework.TestCase;
import models.PlayingField;

import org.junit.Before;
import org.junit.Test;

import ai.WinDetector;

public class WinDetectorTest extends TestCase{

	private PlayingField field;
	private WinDetector detector;
	
	@Before
	public void setUp() throws Exception
	{
		field = new PlayingField();
		detector = new WinDetector();
	}

	@Test
	public void testColumnWinDetection() {
		PreparePlayingFieldForColumnTest();
		detector.setPlayGround(field);
		assertEquals(1, detector.detectWinner());
	}
	
	public void PreparePlayingFieldForColumnTest()
	{
		field.setToken(0, 1);
		field.setToken(0, 2);
		field.setToken(0, 1);
		field.setToken(0, 1);
		field.setToken(0, 1);
		field.setToken(0, 1);
	}

	@Test
	public void testRowWinDetection() {
		PreparePlayingFieldForRowTest();
		detector.setPlayGround(field);
		assertEquals(2, detector.detectWinner());
	}
	
	private void PreparePlayingFieldForRowTest()
	{
		field.setToken(0, 2);
		field.setToken(1, 1);
		field.setToken(2, 2);
		field.setToken(3, 2);
		field.setToken(4, 2);
		field.setToken(5, 2);
	}@Test
	
	public void testDiagonalWinDetection() {
		PreparePlayingFieldForDiagonalTest();
		detector.setPlayGround(field);
		assertEquals(1, detector.detectWinner());
	}
	
	private void PreparePlayingFieldForDiagonalTest()
	{
		field.setToken(0, 2);
		field.setToken(0, 1);
		field.setToken(0, 2);
		field.setToken(0, 1);
		field.setToken(1, 1);
		field.setToken(1, 2);
		field.setToken(1, 1);
		field.setToken(2, 2);
		field.setToken(2, 1);
		field.setToken(3, 1);
	}

}
