/**
 * 
 */
/**
 * In this package we tested some critical lines of code. We focused on our
 * model- and interface-classes to ensure that the functionality is given when
 * we change the code in these classes. <br>
 * <br>
 * <br>
 * In {@link tests.PlayingFieldTest} we tested the insertion of a new Token.
 * This is the most important function of this model-class.<br>
 * <br>
 * In {@link tests.WinDetectorTest} we ensure the functionality that we
 * identify a winner on a given field. We only need to test one function
 * {@link ai.WinDetector#detectWinner()} to see whether it works correctly.<br>
 * <br>
 * The {@link tests.SimulatorTest} test-class was implemented to test the
 * detection of possible columns to throw in a Token.<br>
 * <br>
 * <br>
 * 
 * 
 * @author Stefan Weimann
 */
package tests;