package server;

import java.io.File;
import java.util.TimerTask;

/**
 * Watches files for changes
 * 
 * @author MarcusNotheis
 *
 */
public class FileWatcher extends TimerTask {

	private File file;
	private String path;
	private XMLParser xmlParser;

	/**
	 * Creates new FileWatcher with path
	 * 
	 * @param path
	 *            The path where the file should be read from.
	 */
	public FileWatcher(String path) {
		this.path = path;
		File file = new File(path);
		this.file = file;
		xmlParser = new XMLParser();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.TimerTask#run()
	 */
	public void run() {
		if (file.exists()) {
			ServerConnector.updateAvailable = true;			
		}
		else {
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Parses the server XML and returns the new move
	 * 
	 * @return The new enemy move
	 */
	public int onChange() {
		ServerConnector.updateAvailable = false;
		return xmlParser.parseEnemyMoveOfXML(path);
	};
}