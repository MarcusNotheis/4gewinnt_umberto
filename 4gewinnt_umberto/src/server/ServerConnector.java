package server;

import java.io.FileWriter;
import java.io.IOException;

/**
 * The ServerConnector manages all communication with the server. Once the XML
 * file which is received from the server and twice writing the text file on the
 * server.
 * 
 * @author Heußner
 * 
 */
public class ServerConnector {

	public static Boolean updateAvailable = false;
	private FileWriter writer;
	private int move = -1;
	private FileWatcher fileWatcher = null;
	String path;
	String serverPlayerName;

	/**
	 * Sets the path of the directory of the coming game.
	 * 
	 * @param path
	 *            Path to server
	 * @param serverPlayerName
	 *            "x" or "o" depending on what is configured on the server. Is
	 *            needed to open and write the right files for the match.
	 */
	public void setPath(String path, String serverPlayerName) {
		this.path = path;
		this.serverPlayerName = serverPlayerName;
		String pathWithFile = path + "server2spieler" + serverPlayerName
				+ ".xml";
		fileWatcher = new FileWatcher(pathWithFile);
		System.out.println("Server Connector: Game Path: " + path);

	}

	/**
	 * Loop for check for server update
	 * 
	 * @return The detected move of the enemy. -1 means Umberto starts. 0 till 6
	 *         represents the last enemy move.
	 */
	public int checkForUpdate() {
		System.out
				.println("Server Connector: Start Check for Enemy Move------");
		// set move to -1 to make sure that move is invalid if there is no
		// change detected

		while (!updateAvailable) {
			fileWatcher.run();
		}
		move = fileWatcher.onChange();
		if (move != -1) {
			System.out.println("Server Connector: enemy move found in column: "
					+ move);
		}
		System.out.println("Server Connector: Enemy Move: " + move);
		System.out
				.println("Server Connector: End   Check for Enemy Move------");

		return move;
	}

	/**
	 * Saves calculated move to server
	 * 
	 * @param nextMove
	 *            The calculated move of our KI, which has to be saved on the
	 *            server.
	 * @throws IOException
	 *             Raised if the FileWriter fails
	 */
	public void saveMove(int nextMove) throws IOException {
		// Datei mit Zahl zwischen 0 und 6 (nextMove) in Pfad abspeichern
		String nextMoveSt = "" + nextMove;
		try {
			writer = new FileWriter(path + "spieler" + serverPlayerName
					+ "2server.txt");
			System.out.println("Server Connector: File writer created...");
		} catch (IOException e) {
			System.err
					.println("Server Connector: Failed to create FileWriter for draw");
		}
		writer.write(nextMoveSt);
		writer.flush();
		System.out.println("Server Connector: Saved move in column: "
				+ nextMove + " in new File on Server");
		writer.close();
	}
}
