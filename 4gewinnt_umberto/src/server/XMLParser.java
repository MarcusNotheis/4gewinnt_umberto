package server;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * Class for reading data out of XML files
 * 
 * @author Marcus Notheis
 *
 */
public class XMLParser {

	/**
	 * Gets the enemy move out of the given XML file
	 * 
	 * @param inputFile
	 *            The path to the XML file
	 * @return The detected move
	 */
	public int parseEnemyMoveOfXML(String inputFile) {

		String enemyMove = "";
		File serverFile = new File(inputFile);
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			Document doc = builder.parse(serverFile);

			NodeList list = doc.getElementsByTagName("gegnerzug");
			enemyMove = list.item(0).getTextContent();
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally {
			serverFile.delete();
		}

		return new Integer(enemyMove);

	}

}
